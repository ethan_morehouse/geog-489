﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;


namespace Lesson3PracticeExercises
{
    public class Project3_Practice4 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Project3_Practice4()
        {
        }

        protected override void OnClick()
        {
        }

        protected override void OnUpdate()
        {
            //Disable first
            Enabled = false;
            
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IEnumLayer pLayers = pMxDoc.FocusMap.Layers;
            ILayer pLayer = pLayers.Next();

            while (pLayer != null)
            {
                //Enable button and break loop as soon as a Feature Layer is found
                if (pLayer is IFeatureLayer)
                {
                    Enabled = true;
                    break;
                }
                pLayer = pLayers.Next();
            }
        }
    }
}
