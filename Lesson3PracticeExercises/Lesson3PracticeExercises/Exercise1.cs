﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;


namespace Lesson3PracticeExercises
{
    public class Exercise1 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Exercise1()
        {
        }

        protected override void OnClick()
        {
            Practice1Form form = new Practice1Form();

            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMaps pMaps = pMxDoc.Maps;
            IMap pMap;

            for (int i = 0; i < pMaps.Count; i++)
            {
                pMap = pMaps.get_Item(i);
                form.cboDataFrames.Items.Add(pMap.Name);
            }

            form.ShowDialog();
        }

        protected override void OnUpdate()
        {
        }
    }
}
