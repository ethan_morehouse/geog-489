﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;

namespace Lesson3PracticeExercises
{
    public partial class Practice1Form : Form
    {
        public Practice1Form()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!(this.cboDataFrames.SelectedIndex >= 0))
            {
                this.Close();
            }

            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMaps pMaps = pMxDoc.Maps;
            IMap pMap;

            for (int i = 0; i < pMaps.Count; i++)
            {
                if (this.cboDataFrames.SelectedIndex == i)
                {
                    pMap = pMaps.get_Item(i);
                    pMxDoc.ActiveView = (IActiveView)pMap;
                    break;
                }
            }
            this.Close();
        }

    }
}
