﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;


namespace Lesson3PracticeExercises
{
    public class Exercise3 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Exercise3()
        {
        }

        protected override void OnClick(){

            procedure();
        }

        protected override void OnUpdate()
        {
        }
        private void procedure()
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IEnumLayer pLayers = pMxDoc.FocusMap.Layers;
            pLayers.Reset();
            ILayer pLayer = pLayers.Next();
            while (pLayer != null)
            {
                if (pLayer.Name == "us_cities" || pLayer.Name == "U.S Cities")
                {
                    IGeoFeatureLayer pGeoFLayer = (IGeoFeatureLayer)pLayer;

                    pGeoFLayer.Renderer = (IFeatureRenderer)GetSimpleMarkerRenderer(64, 150, 30, esriSimpleMarkerStyle.esriSMSCross);
                    break;
                }
                pLayer = pLayers.Next();
                
            }
            pMxDoc.UpdateContents();
            pMxDoc.ActiveView.Refresh();
            ICmykColor pCmykColor = new CmykColor();
            pCmykColor.UseWindowsDithering = true;
        }

        private ISimpleRenderer GetSimpleMarkerRenderer(int red, int green, int blue, esriSimpleMarkerStyle marker)
        {

            ISimpleRenderer pSimpleRenderer = new SimpleRenderer();
            pSimpleRenderer.Symbol = (ISymbol)GetMarkerSymbol(red, green, blue, marker);

            return pSimpleRenderer;
        }

        private ICmykColor cmyk() 
        {
            ICmykColor pCmykColor = new CmykColor();
            pCmykColor.Magenta = 50;
            return pCmykColor;
        }

        private IRgbColor GetColor(int red, int green, int blue)
        {
            IRgbColor pRGBColor = new RgbColor();
            pRGBColor.Red = red;
            pRGBColor.Green = green;
            pRGBColor.Blue = blue;
            return pRGBColor;
        }

        private ISimpleMarkerSymbol GetMarkerSymbol(int red, int green, int blue, esriSimpleMarkerStyle marker)
        {
            //IRgbColor pRGBColor = GetColor(red, green, blue);
            ICmykColor pCmykColor = cmyk();

            ISimpleMarkerSymbol pSym = new SimpleMarkerSymbol();
            pSym.Color = pCmykColor;
            pSym.Style = marker;
            pSym.Size = 5;

            return pSym;
        }
    }
}
