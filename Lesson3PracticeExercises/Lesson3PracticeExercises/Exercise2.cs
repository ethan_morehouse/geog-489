﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.DataSourcesFile;
using ESRI.ArcGIS.Catalog;
using ESRI.ArcGIS.CatalogUI;


namespace Lesson3PracticeExercises
{
    public class Exercise2 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Exercise2()
        {
        }

        protected override void OnClick()
        {
            alternate();
        }

        protected override void OnUpdate()
        {
        }

        private void procedure()
        {

            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap pMap = pMxDoc.FocusMap;

            IFeatureLayer pFLayer = new FeatureLayer();

            IWorkspaceFactory pWSFactory = new ShapefileWorkspaceFactory();
            IWorkspace pWorkspace = pWSFactory.OpenFromFile("C:/Users/E/Desktop/PSU/GEOG 489/Lesson 1_2 Data/", ArcMap.Application.hWnd);

            IFeatureWorkspace pFWorkspace = (IFeatureWorkspace)pWorkspace;

            IFeatureClass pFClass = pFWorkspace.OpenFeatureClass("us_boundaries.shp");
            pFLayer.FeatureClass = pFClass;
            pFLayer.Name = "U.S States";

            pMap.AddLayer(pFLayer);

            IFeatureClass pFClass1 = pFWorkspace.OpenFeatureClass("us_cities.shp");
            pFLayer.FeatureClass = pFClass1;
            pFLayer.Name = "U.S Cities";

            pMap.AddLayer(pFLayer);
        }

        private void alternate()
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap pMap = pMxDoc.FocusMap;

            GxDialog dialog = new GxDialogClass();
            dialog.AllowMultiSelect = true;
            dialog.Title = "Add Layer Window";
            dialog.RememberLocation = true;

            IGxObjectFilter pGxObjectFilter = new GxFilterShapefiles();
            dialog.ObjectFilter = pGxObjectFilter;

            IEnumGxObject pGxObjects;
            dialog.DoModalOpen(ArcMap.Application.hWnd, out pGxObjects);

            pGxObjects.Reset();
            IGxObject pGxObject = pGxObjects.Next();

            //is there a valid pGxObject?
            if (pGxObject == null)
            {
                return;
            }

            IWorkspaceFactory pWSFactory = new ShapefileWorkspaceFactory();

            //get Workspace location
            string workspace = FindWorkspace(pGxObject.FullName);
            IWorkspace pWorkspace = pWSFactory.OpenFromFile(workspace, ArcMap.Application.hWnd);
            IFeatureWorkspace PFWorkspace = (IFeatureWorkspace)pWorkspace;

            //cycle through GxObjects and add to map.
            while (pGxObject != null)
            {
                IFeatureClass pFClass = PFWorkspace.OpenFeatureClass(pGxObject.Name);
                IFeatureLayer pFLayer = new FeatureLayer();
                pFLayer.FeatureClass = pFClass;
                pFLayer.Name = pGxObject.BaseName;
                pMap.AddLayer(pFLayer);

                pFClass = null;
                pFLayer = null;

                pGxObject = pGxObjects.Next();
            }
            
        }

        //extracts workspace from FullName Attribute of first object
        private string FindWorkspace(string fullpath)
        {
            char delim = '\\';
            string[] strArraySplitPath = fullpath.Split(delim);
            int charNum = strArraySplitPath[strArraySplitPath.Length - 1].Length;
            string returnValue = fullpath.Substring(0, fullpath.Length - charNum);
            return returnValue;
        }
    }
}
