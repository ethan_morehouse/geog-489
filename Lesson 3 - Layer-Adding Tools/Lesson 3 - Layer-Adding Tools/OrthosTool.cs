﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;

namespace Lesson_3___Layer_Adding_Tools
{
    public class OrthosTool : ESRI.ArcGIS.Desktop.AddIns.Tool
    {
        public OrthosTool()
        {
        }

        protected override void OnUpdate()
        {
            Enabled = false;

            IFeatureLayer pFlayer = Methods.GetFeatureLayer("orth_idx");
            if (pFlayer != null)
            {
                Enabled = true;
            }
        }

        protected override void OnMouseUp(ESRI.ArcGIS.Desktop.AddIns.Tool.MouseEventArgs arg)
        {
            IPoint pPoint = Methods.onclick();
            IFeatureLayer pFLayer = Methods.GetFeatureLayer("orth_idx");
            string layerName = Methods.GetLayerName(pPoint, pFLayer, "ORTHOID");
            Methods.AddOrtho(layerName);
        }
    }

}
