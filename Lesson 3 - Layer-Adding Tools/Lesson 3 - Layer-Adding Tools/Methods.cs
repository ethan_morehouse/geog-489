﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.DataSourcesFile;
using ESRI.ArcGIS.DataSourcesRaster;
using ESRI.ArcGIS.Display;
using System.Diagnostics;

//Created by Ethan Morehouse - 6/20/2015
// GEOG 489

namespace Lesson_3___Layer_Adding_Tools
{
    class Methods
    {

        static IMxDocument pMxDoc;


        //searches for Index layer based on name and returns layer
        public static IFeatureLayer GetFeatureLayer(string indexName)
        {
            pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IEnumLayer pLayers = pMxDoc.FocusMap.Layers;
            pLayers.Reset();

            ILayer pLayer = pLayers.Next();

            while (pLayer != null)
            {
                if (pLayer is IFeatureLayer && pLayer.Name == indexName)
                {
                    IFeatureLayer pFLayer = (IFeatureLayer)pLayer;
                    return pFLayer;
                }

                pLayer = pLayers.Next();
            }
            return null;
        }


        //get click location
        public static IPoint onclick()
        {
            IPoint pPoint = pMxDoc.CurrentLocation;
            return pPoint;
        }


        //cursors through index layer and extracts feature layer name to be added.
        public static string GetLayerName (IPoint pPoint, IFeatureLayer pFLayer, string strField) 
        {
            string layerName;

            ISpatialFilter pSpatialFilter = new SpatialFilter();
            pSpatialFilter.Geometry = pPoint;
            pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;

            IFeatureCursor pFCursor = pFLayer.Search(pSpatialFilter, true);

            IFeature pFeature = pFCursor.NextFeature();

            if (pFeature != null)
            {
                layerName = pFeature.Value[pFCursor.FindField(strField)].ToString();
            }
            else 
            {
                layerName = "Undefined";
            }

            return layerName;
        }


        //adds shapefile to active data frame.
        public static void AddShapefile(string NEWID)
        {
            string layerName = "roads_" + NEWID;
            string filename = layerName+".shp";

            //check to see if shapefile exists
            if (!System.IO.File.Exists("c:/Lesson3/roads/"+filename))
            {
                return;
            }

            //don't want to add multiple instances of the same shapefile
            if (doesLayerAlreadyExist(layerName))
            {
                return;
            }

            IMap pMap = pMxDoc.FocusMap;

            IFeatureLayer pFLayer = new FeatureLayer();
            IWorkspaceFactory pWSFactory = new ShapefileWorkspaceFactory();
            IWorkspace pWorkspace = pWSFactory.OpenFromFile("c:/Lesson3/roads", ArcMap.Application.hWnd);

            IFeatureWorkspace pFWorkspace = (IFeatureWorkspace)pWorkspace;
            IFeatureClass pFClass = pFWorkspace.OpenFeatureClass(filename);

            pFLayer.FeatureClass = pFClass;
            pFLayer.Name = layerName;

            //set custom symbology
            IRgbColor pColor = new RgbColor();
            pColor.Red = 255;
            pColor.Green = 255;
            pColor.Blue = 0;

            ISimpleLineSymbol pSLSymbol = new SimpleLineSymbol();
            pSLSymbol.Color = pColor;
            pSLSymbol.Style = esriSimpleLineStyle.esriSLSDash;
            pSLSymbol.Width = 1;

            ISimpleRenderer pSimpleRenderer = new SimpleRenderer();
            pSimpleRenderer.Symbol = (ISymbol)pSLSymbol;

            IGeoFeatureLayer pGFLayer = (IGeoFeatureLayer)pFLayer;
            pGFLayer.Renderer = (IFeatureRenderer)pSimpleRenderer;

            //add layer
            pMap.AddLayer(pGFLayer);
        }

        //adds ortho to active data frame.
        public static void AddOrtho(string name)
        {
            name = name + ".tif";
            string filepath = "c:\\Lesson3\\orthos\\"+ name;

            //check to see if the ortho exists
            if (!System.IO.File.Exists(filepath))
            {
                return;
            }
            
            //don't want to add multiple instances of the same ortho
            if (doesLayerAlreadyExist(name))
            {
                return;
            }

            IMap pMap = pMxDoc.FocusMap;

            IRasterLayer pRLayer = new RasterLayer();

            pRLayer.CreateFromFilePath(filepath);

            //add ortho
            pMap.AddLayer(pRLayer);
        }

        //checks to see if the layer about to be added already exists in the data frame.
        public static bool doesLayerAlreadyExist(string layerName)
        {
            IEnumLayer pLayers = pMxDoc.FocusMap.get_Layers();
            pLayers.Reset();
            ILayer pLayer = pLayers.Next();

            //cycle through layers
            while (pLayer != null)
            {
                if (pLayer.Name == layerName)
                {
                    //Layer already exists
                    return true;
                }
                pLayer = pLayers.Next();
            }
            return false;
        }
    }
}
#endregion