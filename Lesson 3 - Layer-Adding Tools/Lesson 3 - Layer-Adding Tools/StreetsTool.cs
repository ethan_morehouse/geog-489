﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Display;
using ESRI.ArcGIS.Geometry;

namespace Lesson_3___Layer_Adding_Tools
{
    public class StreetsTool : ESRI.ArcGIS.Desktop.AddIns.Tool
    {

        public StreetsTool()
        {
        }

        protected override void OnUpdate()
        {
            Enabled = false;

            IFeatureLayer pFLayer = Methods.GetFeatureLayer("road_idx");
            if (pFLayer != null)
            {
                Enabled = true;
            }


        }

        protected override void OnMouseUp(ESRI.ArcGIS.Desktop.AddIns.Tool.MouseEventArgs arg)
        {
            IPoint pPoint = Methods.onclick();
            IFeatureLayer pFLayer = Methods.GetFeatureLayer("road_idx");
            string layerName = Methods.GetLayerName(pPoint, pFLayer, "NEWID");
            Methods.AddShapefile(layerName);
            
        }
    }
}
