﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;


namespace Lesson2
{
    public class Exercise2 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Exercise2()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        private void procedure()
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;

            //get data frames
            IMaps pMaps = pMxDoc.Maps;
            IMap pMap;

            //cycle through data frames and expand / collapse
            for (int i = 0; i < pMaps.Count; i++)
            {
                pMap = pMaps.get_Item(i);
                if (pMap.Expanded)
                {
                    pMap.Expanded = false;
                }
                else
                {
                    pMap.Expanded = true;
                }
            }

            //update TOC
            pMxDoc.UpdateContents();
        }
    }
}
