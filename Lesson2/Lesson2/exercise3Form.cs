﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lesson2
{
    public partial class exercise3Form : Form
    {
        public exercise3Form()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e) //get input string, set to upper or lower, and pass to Exercise3.StringManipulator
        {
            string strUserInput = this.txtUserInput.Text;

            if (this.rbtUpper.Checked)
            {
                Exercise3.StringManipulator(strUserInput.ToUpper());
            }
            else
            {
                Exercise3.StringManipulator(strUserInput.ToLower());
            }
            this.Close();

        }
    }
}
