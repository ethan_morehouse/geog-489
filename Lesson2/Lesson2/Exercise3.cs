﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.VisualBasic;
using System.Windows.Forms;


namespace Lesson2
{
    public class Exercise3 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Exercise3()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        private void procedure() //create new form
        {
            exercise3Form form = new exercise3Form();
            form.Show();

        }

        public static void StringManipulator(string input) //Notify user of substring result
        {
            string strSub = input.Substring(0, 3);
            MessageBox.Show(strSub);
        }
    }
}
