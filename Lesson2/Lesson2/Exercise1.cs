﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.VisualBasic;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;



namespace Lesson2
{
    public class Exercise1 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Exercise1()
        {
        }

        protected override void OnClick()
        {
            getLayer();
        }

        protected override void OnUpdate()
        {
            Enabled = ArcMap.Application != null;
        }


        public static void FieldCounter(string strSelectedLayer)  // called from exercise1Form btnOK On Click event:  Accepts users selected layer from form
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap pMap = pMxDoc.FocusMap;
            IEnumLayer pLayers = pMap.get_Layers();
            pLayers.Reset();
            ILayer pLayer = pLayers.Next();

            while (pLayer != null)
            {
                if (pLayer.Name == strSelectedLayer)
                {
                    ITableFields pTableFields = (ITableFields)pLayer;
                    Interaction.MsgBox(string.Format("{0} has {1} fields.", strSelectedLayer, pTableFields.FieldCount));
                    break;
                }

                pLayer = pLayers.Next();
            }
            
            
        }

        private void getLayer() //Create new form and populate combobox with layers that inherit from TableFields
        {
            exercise1Form form = new exercise1Form();
            
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap pMap = pMxDoc.FocusMap;
            IEnumLayer pLayers = pMap.get_Layers();
            pLayers.Reset();
            ILayer pLayer = pLayers.Next();

            while (pLayer != null) 
            {
                if(pLayer is ITableFields)
                {
                    form.cboLayers.Items.Add(pLayer.Name);
                }
                pLayer = pLayers.Next();
            }
            form.Show();
        }
    }

}
