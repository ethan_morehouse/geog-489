﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lesson2
{
    public partial class exercise1Form : Form
    {
        public exercise1Form()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string strSelectedLayer = this.cboLayers.SelectedItem.ToString();
            Exercise1.FieldCounter(strSelectedLayer);
            this.Close();
        }
    }
}
