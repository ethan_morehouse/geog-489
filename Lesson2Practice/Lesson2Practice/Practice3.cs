﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;


namespace Lesson2Practice
{
    public class Practice3 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Practice3()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        private void procedure()
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap pMap = pMxDoc.FocusMap;

            string strName;
            long lngNumLayers;

            strName = pMap.Name;
            lngNumLayers = pMap.LayerCount;

            MessageBox.Show("The name of the active data frame is " + strName + "\nIt has " + lngNumLayers + " layers.");
        }
    }
}
