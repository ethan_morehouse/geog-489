﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Microsoft.VisualBasic;


namespace Lesson2Practice
{
    public class Practice14 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Practice14()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        private void procedure()
        {
            double f1 = getAndConvert("3AM");
            double f2 = getAndConvert("9AM");
            double f3 = getAndConvert("3PM");
            double f4 = getAndConvert("9PM");

            double fAvg = (f1 + f2 + f3 + f4) / 4;
            MessageBox.Show("The average temperature of the day was " + fAvg.ToString());
        }

        private double getAndConvert(string time)
        {
            string strUserInput = Interaction.InputBox(string.Format("Please enter the {0} temp in celsius.", time));
            double dblUserInput;
            if (double.TryParse(strUserInput, out dblUserInput))
            {
                double dblFTemp = ((9 / 5.0) * dblUserInput) + 32;
                return dblFTemp;
            }
            else
            {
                return getAndConvert(time);
            }
        }
    }
}
