﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;


namespace Lesson2Practice
{
    public class Practice4 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Practice4()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        private void procedure()
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap pMap = pMxDoc.FocusMap;
            IEnumLayer pLayers = pMap.get_Layers();
            ILayer pLayer = pLayers.Next();
            while (pLayer != null)
            {
                if (pLayer.Name == "us_boundaries")
                {
                    pLayer.Name = "U.S Boundaries";
                }
                pLayer = pLayers.Next();
            }
            pMxDoc.UpdateContents();
        }
    }
}
