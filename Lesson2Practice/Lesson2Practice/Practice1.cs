﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Lesson2Practice
{
    public class Practice1 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Practice1()
        {
        }

        protected override void OnClick()
        {
            procedure();
            ArcMap.Application.CurrentTool = null;
        }
        protected override void OnUpdate()
        {
            Enabled = ArcMap.Application != null;
        }
        private void procedure()
        {
            ArcMap.Application.Caption = "We are Penn State!";
        }
    }

}
