﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Microsoft.VisualBasic;


namespace Lesson2Practice
{
    public class Practice12 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Practice12()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        private void procedure()
        {
            string strUserInput = Interaction.InputBox("Please enter a number:");

            if (string.IsNullOrEmpty(strUserInput))
            {
                return;
            }

            try
            {
                double dblUserInput = Convert.ToDouble(strUserInput);
                double dblSquare = dblUserInput * dblUserInput;

                MessageBox.Show(string.Format("{0} squared is {1}", dblUserInput, dblSquare));
            }
            catch
            {
                MessageBox.Show(string.Format("{0} is not a valid entry.  Please try again.", strUserInput));
                procedure();
            }
        }
    }
}
