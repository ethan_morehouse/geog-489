﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;


namespace Lesson2Practice
{
    public class Practice11 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Practice11()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        private void procedure()
        {   
            string strPrinterNames = null;
            IEnumPrinterNames pPrinterNames = (IEnumPrinterNames)ArcMap.Application;
            string strPrinterName = pPrinterNames.Next();
            

            while (!string.IsNullOrEmpty(strPrinterName))
            {
                strPrinterNames += strPrinterName + "\n";
                strPrinterName = pPrinterNames.Next();
            }

            MessageBox.Show(string.Format("The following printers are available to you:\n\n{0}", strPrinterNames));
        }
    }
}
