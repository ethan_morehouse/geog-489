﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;


namespace Lesson2Practice
{
    public class Practice9 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Practice9()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        private void procedure()
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap pMap = pMxDoc.FocusMap;

            ILayer pLayer = pMap.get_Layer(0);
            ILayerEffects pLayerEffects = (ILayerEffects)pLayer;
            pLayerEffects.Transparency = 50;

            IActiveView pActiveView = (IActiveView)pMap;
            pActiveView.Refresh();
        }
    }
}
