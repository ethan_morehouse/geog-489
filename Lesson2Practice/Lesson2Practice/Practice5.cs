﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;


namespace Lesson2Practice
{
    public class Practice5 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Practice5()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        private void procedure()
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap pMap = pMxDoc.FocusMap;
            MessageBox.Show(string.Format("The map units of the currently active data frame are: {0}", pMap.MapUnits));
        }
    }
}
