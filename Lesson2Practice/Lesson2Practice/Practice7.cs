﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;


namespace Lesson2Practice
{
    public class Practice7 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Practice7()
        {
        }

        protected override void OnClick()
        {
        }

        protected override void OnUpdate()
        {
        }

        private void procedure()
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap pMap = pMxDoc.FocusMap;
            ILayer pLayer = pMap.get_Layer(0);
            string strMapTipsBool;
            if (pLayer.ShowTips)
            {
                strMapTipsBool = "on";
            }
            else
            {
                strMapTipsBool = "off";
            }
            MessageBox.Show(string.Format("The map tips property for the {0} layer is currently {1}", pLayer.Name, strMapTipsBool));
        }
    }
}
