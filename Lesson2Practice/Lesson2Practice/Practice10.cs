﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Microsoft.VisualBasic;


namespace Lesson2Practice
{
    public class Practice10 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Practice10()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        private void procedure()
        {
            string strUserInput = Interaction.InputBox("Please enter a numberical score from 0 - 100:");
            string strLetterGrade;
            try
            {
                if(string.IsNullOrEmpty(strUserInput))
                {
                    return;
                }
                double dblUserInput = Convert.ToDouble(strUserInput);
                if (dblUserInput < 60)
                {
                    strLetterGrade = "F";
                }
                else if (dblUserInput < 70)
                {
                    strLetterGrade = "D";
                }
                else if (dblUserInput < 80)
                {
                    strLetterGrade = "C";
                }
                else if (dblUserInput < 90)
                {
                    strLetterGrade = "B";
                }
                else
                {
                    strLetterGrade = "A";
                }
                MessageBox.Show(string.Format("{0} is a {1}", dblUserInput, strLetterGrade));
            }
            catch 
            {
                MessageBox.Show(string.Format("Your entry: {0} is not a valid entry.  Please press okay and try again.", strUserInput));
                procedure();
            }
        }
    }
}
