﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.SystemUI;
using ESRI.ArcGIS.Catalog;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Framework;

namespace ArcMapAddin1
{
    public partial class FrequentLayerForm : Form
    {
        //system paths to csv and directory to store layers
        public string csvPath = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "ArcGIS"), "FrequentDataLayers.csv");
        public string tempCSVPath = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "ArcGIS"), "FrequentDataLayersTemp.csv");
        public string layerDir = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "ArcGIS"), "FrequentLayers");
        public char[] delim = { ',' };

        public FrequentLayerForm()
        {
            InitializeComponent();

        }

        #region "Replace Layers Button: Adds all the layers in the current data frame to the list of 'frequently used layers', Overwrites existing csv, and adds layers to frequent layers directory"
        private void btnAddLayers_Click(object sender, EventArgs e)
        {
            this.clbLayers.Items.Clear();

            IMxDocument mxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap map = mxDoc.FocusMap;

            //Create new csv for storing the frequently used data's name and filepath
            System.IO.FileStream fs = new System.IO.FileStream(csvPath, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);
            try
            {
                //Iterate through layers
                for(int i = 0; i < map.LayerCount; i++)
                {

                    ILayer layer = map.get_Layer(i);
                    //Concatenate the save directory and the layer name
                    string savePath = layerDir + "\\" + layer.Name + ".lyr";

                    //Add entry to csv
                    sw.WriteLine(layer.Name + "," + savePath);

                    //add Item to Check List Box.
                    this.clbLayers.Items.Add(layer.Name);

                    //save Layer
                    SaveToLayerFile(savePath, layerDir, layer);
                }
            }
            finally
            {
                //close writer and file stream
                sw.Close();
                fs.Close();
            }
            addBtnEnabledCheck();

        }
        #endregion


        #region "Okay Button: Adds selected Layers into map"
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (File.Exists(csvPath))
            {
                //add all checked items to list and define csv delimiter
                List<string> layersToAdd = this.clbLayers.CheckedItems.OfType<string>().ToList();

                if (layersToAdd.Count < 1)
                {
                    return;
                }

                IMxDocument mxDoc = (IMxDocument)ArcMap.Application.Document;
                IMap map = mxDoc.FocusMap;
                IActiveView activeView = (IActiveView)map;

                //open file and reader stream
                System.IO.FileStream fs = new System.IO.FileStream(csvPath, FileMode.Open);
                StreamReader rs = new StreamReader(fs);
                try
                {
                    //read csv line
                    string line = rs.ReadLine();

                    while (!string.IsNullOrEmpty(line))
                    {
                        //split line by delimter and put two substrings in array
                        string[] nameAndPath = line.Split(delim, 2);

                        //see if layers is amongst layers checked
                        if (layersToAdd.Contains(nameAndPath[0]))
                        {
                            //Add layer to active view
                            AddLayerToActiveView(activeView, nameAndPath[1]);
                        }

                        //read next line
                        line = rs.ReadLine();
                    }
                }
                finally
                {
                    //close file and reader stream
                    rs.Close();
                    fs.Close();
                }
                this.Close();

                /////////////// Auto-Arrange layers

                ////pass layers to layer enumerator
                //IEnumLayer layers = (IEnumLayer)map.get_Layers();
                //layers.Reset();

                //ILayer layer = layers.Next();

                ////delete all layers in data frame
                //while (layer != null)
                //{
                //    map.DeleteLayer(layer);
                //    layer = layers.Next();
                //}
                ////add layers back into data frame with auto arrange
                //layers.Reset();
                //map.AddLayers(layers, true);
                //activeView.Refresh();
            }

        }
        #endregion


        #region "Cancel button: closes form"
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion


        #region "Clear All Button: Removes all items from checkboxlist and deletes csv and layers (with all subfiles)"
        private void btnClearAll_Click(object sender, EventArgs e)
        {
            //clears out frequent layers

            //delete csv
            if (File.Exists(csvPath))
            {
                File.Delete(csvPath);
            }
            if (Directory.Exists(layerDir))
            {
                Directory.Delete(layerDir, true);
            }
            

            //clear checked list box items
            this.clbLayers.Items.Clear();

            addBtnEnabledCheck();
        }
        #endregion


        #region "Select all button: Select all items in checkboxlist"
        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.clbLayers.Items.Count; i++)
            {
                this.clbLayers.SetItemCheckState(i, CheckState.Checked);
            }
        }
        #endregion


        #region "Deselect all button: Deselect all items in checkboxlist"
        private void btnDeselectAll_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.clbLayers.Items.Count; i++)
            {
                this.clbLayers.SetItemCheckState(i, CheckState.Unchecked);
            }
        }
        #endregion


        #region "Remove the currently selected items in the list from the csv, layer directory, and the checkboxlist"
        private void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            if(File.Exists(csvPath)) 
            {
                List<string> layersToRemove = this.clbLayers.CheckedItems.OfType<string>().ToList();

                System.IO.FileStream fs = new System.IO.FileStream(csvPath, FileMode.Open);
                StreamReader rs = new StreamReader(fs);
                System.IO.FileStream tempStream = new System.IO.FileStream(tempCSVPath, FileMode.Create);
                StreamWriter ws = new StreamWriter(tempStream);
                string line = rs.ReadLine();
                this.clbLayers.Items.Clear();
                while (!string.IsNullOrEmpty(line))
                {
                    //split line into layer name and layer path
                    string[] nameAndPath = line.Split(delim, 2);

                    //check to see if layer is to be removed
                    if (layersToRemove.Contains(nameAndPath[0]))
                    {
                        //Don't copy line and delete file
                        File.Delete(nameAndPath[1]);
                    }
                    else
                    {
                        //copy line to temp.csv and add item to check box list
                        ws.WriteLine(line);
                        this.clbLayers.Items.Add(nameAndPath[0]);
                    }

                    //read next line
                    line = rs.ReadLine();
                }

                //close file, reader, and writer streams
                rs.Close();
                fs.Close();
                ws.Close();
                tempStream.Close();

                //delete old csv
                File.Delete(csvPath);
                //rename temp csv
                File.Move(tempCSVPath, csvPath);
            }
            addBtnEnabledCheck();
        }
        #endregion


        #region "Append Button: Appends layers in current data frame to list of'frequently used layers'"
        private void btnAppend_Click(object sender, EventArgs e)
        {
            
            IMxDocument mxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap map = mxDoc.FocusMap;
            IEnumLayer layers = map.get_Layers();
            layers.Reset();
            ILayer layer = layers.Next();
            
            //open file and writer stream
            System.IO.FileStream fs = new System.IO.FileStream(csvPath, FileMode.Append);
            StreamWriter sw = new StreamWriter(fs);

            //Iterate over layers
            try
            {
                for(int i = 0; i < map.LayerCount; i++)
                {
                    //only append if layer doesn't exist.  Don't want duplicate layers.
                    if (!this.clbLayers.Items.Contains(map.get_Layer(i).Name))
                    {
                        ILayer pLayer = map.get_Layer(i);

                        //Concatenate the save directory and the layer name
                        string savePath = layerDir + "\\" + pLayer.Name + ".lyr";

                        //Add entry to csv
                        sw.WriteLine(pLayer.Name + "," + savePath);

                        //add Item to Check List Box.
                        this.clbLayers.Items.Add(pLayer.Name);

                        //save Layer
                        SaveToLayerFile(savePath, layerDir, pLayer);
                    }

                    //next layer
                    layer = layers.Next();
                }
            }
            finally 
            {
                //close streams
                sw.Close();
                fs.Close();
            }

            //check to see if the button to add layers to the map should be enabled
            addBtnEnabledCheck();
        }
        #endregion


        #region "Snippet: Save Layer To File"

        ///<summary>Write and Layer to a file on disk.</summary>
        ///  
        ///<param name="layerFilePath">A System.String that is the path and filename for the layer file to be created. Example: "C:\temp\cities.lyr"</param>
        ///<param name="layer">An ILayer interface.</param>
        ///   
        ///<remarks></remarks>
        public void SaveToLayerFile(String layerFilePath, string dirPath, ILayer layer)
        {
            if(layer == null)
            {
                return;
            }
            //create a new LayerFile instance
            ILayerFile layerFile = new LayerFileClass();

            //make sure that the layer file name is valid
            if (System.IO.Path.GetExtension(layerFilePath) != ".lyr")
                return;
            if (!Directory.Exists(dirPath)) 
            {
                Directory.CreateDirectory(dirPath);
            }
            if (layerFile.get_IsPresent(layerFilePath))
                    System.IO.File.Delete(layerFilePath);

            //create a new layer file
            layerFile.New(layerFilePath);

            //attach the layer file with the actual layer
            layerFile.ReplaceContents(layer);

            //save the layer file
            layerFile.Save();
        }
        #endregion


        #region "Snippet: Add Layer File to ActiveView"

        ///<summary>Add a layer file (.lyr) into the active view when the path (on disk or network) is specified.</summary>
        ///      
        ///<param name="activeView">An IActiveview interface</param>
        ///<param name="layerPathFile">A System.String that is the path\filename of a layer file. Example: "C:\temp\mylayer.lyr".</param>
        ///      
        ///<remarks></remarks>
        public void AddLayerToActiveView(IActiveView activeView, System.String layerPathFile)
        {

          if (activeView == null || layerPathFile == null || !layerPathFile.EndsWith(".lyr"))
          {
            return;
          }
  
          // Create a new GxLayer
          IGxLayer gxLayer = new GxLayerClass();

          IGxFile gxFile = (IGxFile)gxLayer; //Explicit Cast

          // Set the path for where the layerfile is located on disk
          gxFile.Path = layerPathFile;

          // Test if we have a valid layer and add it to the map
          if (!(gxLayer.Layer == null))
          {
            IMap map = activeView.FocusMap;
            map.AddLayer(gxLayer.Layer);
          }
        }
        #endregion


        #region "Tooltips"
        private void btnAppend_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Appends all the layers within the current data frame to the list of 'frequently used layers'";
        }

        private void btnSelectAll_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "checks all items in list";
        }

        private void btnDeselectAll_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Unchecks all items in list";
        }

        private void btnCancel_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Closes this window without adding any layers";
        }

        private void btnAdd_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Adds the currently checked items to the current data frame";
        }

        private void btnClearAll_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Deletes all the previously saved 'frequently used layers'. This action cannot be undone.";
        }

        private void btnRemoveSelected_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Removes the currently selected layers from the list of 'frequently used layers'.  This action cannot be undone.";
        }

        private void btnAddLayers_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Replaces all the previously saved 'frequently used layers' with the layers in the active data frame. This action cannot be undone.";
        }

        private void MouseLeave(object sender, EventArgs e)
        {
            if (this.clbLayers.Items.Count < 1)
            {
                this.textBox1.Text = "You must add layers to the list before you can add them to your map.  Close this window and proceed to add layers to your map then reopen this window and click 'Replace Layers'";
            }
            else
            {
                this.textBox1.Text = "Tooltip:";
            }
        }
        #endregion


        #region "addBtnEnabledCheck(): checks to see if "Okay" button should be enabled"
        private void addBtnEnabledCheck()
        {
            if (this.clbLayers.Items.Count > 0)
            {
                this.btnAdd.Enabled = true;
            }
            else
            {
                this.btnAdd.Enabled = false;
                this.textBox1.Text = "You must add layers to the list before you can add them to your map.  Close this window and proceed to add layers to your map then reopen this window and click 'Replace Layers'";
            }
        }
        #endregion

    }
}
