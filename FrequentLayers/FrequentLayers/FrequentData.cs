﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.VisualBasic;

namespace ArcMapAddin1
{
    public class FrequentData : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        protected override void OnClick()
        {
            //create new form
            FrequentLayerForm form = new FrequentLayerForm();

            try
            {
                //check to see if csv exists
                if (File.Exists(form.csvPath))
                {
                    //csv delimiter
                    char[] delim = { ',' };


                    System.IO.FileStream fs = new System.IO.FileStream(form.csvPath, FileMode.Open);
                    StreamReader rs = new StreamReader(fs);
                    try
                    {
                        string line = rs.ReadLine();
                        while (!String.IsNullOrEmpty(line))
                        {
                            string[] split = line.Split(delim, 2);
                            form.clbLayers.Items.Add(split[0]);
                            line = rs.ReadLine();
                        }
                    }
                    finally
                    {
                        rs.Close();
                        fs.Close();
                    }
                    if (form.clbLayers.Items.Count < 1)
                    {
                        form.btnAdd.Enabled = false;
                        form.textBox1.Text = "You must add layers to the list before you can add them to your map.  Close this window and proceed to add layers to your map then reopen this window and click 'Replace Layers'";
                    }
                    else
                    {
                        form.btnAdd.Enabled = true;
                        form.textBox1.Text = "Tooltip:";
                    }
                }
                form.ShowDialog();
            }
            catch(Exception ex)
            {
                try
                {
                    Interaction.MsgBox("The following error ocurred:\n" + ex);
                }
                finally
                {
                    File.Delete(form.csvPath);
                }
                
            }

        }
        protected override void OnUpdate()
        {
            Enabled = ArcMap.Application != null;
        }
    }

}
