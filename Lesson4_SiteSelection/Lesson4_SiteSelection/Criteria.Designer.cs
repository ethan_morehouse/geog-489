﻿namespace Lesson4_SiteSelection
{
    partial class Criteria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbFarms = new System.Windows.Forms.CheckBox();
            this.cbLabor = new System.Windows.Forms.CheckBox();
            this.cbCrime = new System.Windows.Forms.CheckBox();
            this.cbPopDen = new System.Windows.Forms.CheckBox();
            this.cbCollege = new System.Windows.Forms.CheckBox();
            this.txtFarms = new System.Windows.Forms.TextBox();
            this.txtCrime = new System.Windows.Forms.TextBox();
            this.txtLabor = new System.Windows.Forms.TextBox();
            this.txtPopDen = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cbInterstate = new System.Windows.Forms.CheckBox();
            this.cbRecAreas = new System.Windows.Forms.CheckBox();
            this.txtInterstate = new System.Windows.Forms.TextBox();
            this.txtRec = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cbFarms
            // 
            this.cbFarms.AutoSize = true;
            this.cbFarms.Checked = true;
            this.cbFarms.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFarms.Location = new System.Drawing.Point(6, 36);
            this.cbFarms.Name = "cbFarms";
            this.cbFarms.Padding = new System.Windows.Forms.Padding(10);
            this.cbFarms.Size = new System.Drawing.Size(141, 37);
            this.cbFarms.TabIndex = 0;
            this.cbFarms.Text = "Number of farms    >";
            this.cbFarms.UseVisualStyleBackColor = true;
            this.cbFarms.CheckedChanged += new System.EventHandler(this.cbFarms_CheckedChanged);
            // 
            // cbLabor
            // 
            this.cbLabor.AutoSize = true;
            this.cbLabor.Checked = true;
            this.cbLabor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLabor.Location = new System.Drawing.Point(6, 79);
            this.cbLabor.Name = "cbLabor";
            this.cbLabor.Padding = new System.Windows.Forms.Padding(10);
            this.cbLabor.Size = new System.Drawing.Size(115, 37);
            this.cbLabor.TabIndex = 1;
            this.cbLabor.Text = "Labor Pool    >";
            this.cbLabor.UseVisualStyleBackColor = true;
            this.cbLabor.CheckedChanged += new System.EventHandler(this.cbLabor_CheckedChanged);
            // 
            // cbCrime
            // 
            this.cbCrime.AutoSize = true;
            this.cbCrime.Checked = true;
            this.cbCrime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCrime.Location = new System.Drawing.Point(6, 122);
            this.cbCrime.Name = "cbCrime";
            this.cbCrime.Padding = new System.Windows.Forms.Padding(10);
            this.cbCrime.Size = new System.Drawing.Size(122, 37);
            this.cbCrime.TabIndex = 2;
            this.cbCrime.Text = "Crime Index   <=";
            this.cbCrime.UseVisualStyleBackColor = true;
            this.cbCrime.CheckedChanged += new System.EventHandler(this.cbCrime_CheckedChanged);
            // 
            // cbPopDen
            // 
            this.cbPopDen.AutoSize = true;
            this.cbPopDen.Checked = true;
            this.cbPopDen.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPopDen.Location = new System.Drawing.Point(6, 165);
            this.cbPopDen.Name = "cbPopDen";
            this.cbPopDen.Padding = new System.Windows.Forms.Padding(10);
            this.cbPopDen.Size = new System.Drawing.Size(158, 37);
            this.cbPopDen.TabIndex = 3;
            this.cbPopDen.Text = "Population Density    <=";
            this.cbPopDen.UseVisualStyleBackColor = true;
            this.cbPopDen.CheckedChanged += new System.EventHandler(this.cbPopDen_CheckedChanged);
            // 
            // cbCollege
            // 
            this.cbCollege.AutoSize = true;
            this.cbCollege.Checked = true;
            this.cbCollege.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCollege.Location = new System.Drawing.Point(6, 294);
            this.cbCollege.Name = "cbCollege";
            this.cbCollege.Padding = new System.Windows.Forms.Padding(10);
            this.cbCollege.Size = new System.Drawing.Size(168, 37);
            this.cbCollege.TabIndex = 4;
            this.cbCollege.Text = "Near University or College";
            this.cbCollege.UseVisualStyleBackColor = true;
            // 
            // txtFarms
            // 
            this.txtFarms.Location = new System.Drawing.Point(144, 44);
            this.txtFarms.Name = "txtFarms";
            this.txtFarms.Size = new System.Drawing.Size(100, 20);
            this.txtFarms.TabIndex = 5;
            this.txtFarms.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtboxkeypress);
            // 
            // txtCrime
            // 
            this.txtCrime.Location = new System.Drawing.Point(128, 130);
            this.txtCrime.Name = "txtCrime";
            this.txtCrime.Size = new System.Drawing.Size(100, 20);
            this.txtCrime.TabIndex = 8;
            this.txtCrime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtboxkeypress);
            // 
            // txtLabor
            // 
            this.txtLabor.Location = new System.Drawing.Point(118, 87);
            this.txtLabor.Name = "txtLabor";
            this.txtLabor.Size = new System.Drawing.Size(100, 20);
            this.txtLabor.TabIndex = 7;
            this.txtLabor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtboxkeypress);
            // 
            // txtPopDen
            // 
            this.txtPopDen.Location = new System.Drawing.Point(161, 173);
            this.txtPopDen.Name = "txtPopDen";
            this.txtPopDen.Size = new System.Drawing.Size(100, 20);
            this.txtPopDen.TabIndex = 9;
            this.txtPopDen.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtboxkeypress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(281, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Please modify the site selection criteria below:";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Location = new System.Drawing.Point(15, 341);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "Okay";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(210, 341);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cbInterstate
            // 
            this.cbInterstate.AutoSize = true;
            this.cbInterstate.Checked = true;
            this.cbInterstate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbInterstate.Location = new System.Drawing.Point(6, 208);
            this.cbInterstate.Name = "cbInterstate";
            this.cbInterstate.Padding = new System.Windows.Forms.Padding(10);
            this.cbInterstate.Size = new System.Drawing.Size(182, 37);
            this.cbInterstate.TabIndex = 13;
            this.cbInterstate.Text = "Distance from Interstate    <=";
            this.cbInterstate.UseVisualStyleBackColor = true;
            this.cbInterstate.CheckedChanged += new System.EventHandler(this.cbInterstate_CheckedChanged);
            // 
            // cbRecAreas
            // 
            this.cbRecAreas.AutoSize = true;
            this.cbRecAreas.Checked = true;
            this.cbRecAreas.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbRecAreas.Location = new System.Drawing.Point(6, 251);
            this.cbRecAreas.Name = "cbRecAreas";
            this.cbRecAreas.Padding = new System.Windows.Forms.Padding(10);
            this.cbRecAreas.Size = new System.Drawing.Size(183, 37);
            this.cbRecAreas.TabIndex = 14;
            this.cbRecAreas.Text = "Distance from Rec Area    <=";
            this.cbRecAreas.UseVisualStyleBackColor = true;
            this.cbRecAreas.CheckedChanged += new System.EventHandler(this.cbRecAreas_CheckedChanged);
            // 
            // txtInterstate
            // 
            this.txtInterstate.Location = new System.Drawing.Point(185, 216);
            this.txtInterstate.Name = "txtInterstate";
            this.txtInterstate.Size = new System.Drawing.Size(100, 20);
            this.txtInterstate.TabIndex = 11;
            this.txtInterstate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtboxkeypress);
            // 
            // txtRec
            // 
            this.txtRec.Location = new System.Drawing.Point(186, 259);
            this.txtRec.Name = "txtRec";
            this.txtRec.Size = new System.Drawing.Size(100, 20);
            this.txtRec.TabIndex = 12;
            this.txtRec.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtboxkeypress);
            // 
            // Criteria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 376);
            this.Controls.Add(this.txtRec);
            this.Controls.Add(this.txtInterstate);
            this.Controls.Add(this.cbRecAreas);
            this.Controls.Add(this.cbInterstate);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPopDen);
            this.Controls.Add(this.txtLabor);
            this.Controls.Add(this.txtCrime);
            this.Controls.Add(this.txtFarms);
            this.Controls.Add(this.cbCollege);
            this.Controls.Add(this.cbPopDen);
            this.Controls.Add(this.cbCrime);
            this.Controls.Add(this.cbLabor);
            this.Controls.Add(this.cbFarms);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Criteria";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Site Selection Criteria";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbFarms;
        private System.Windows.Forms.CheckBox cbLabor;
        private System.Windows.Forms.CheckBox cbCrime;
        private System.Windows.Forms.CheckBox cbPopDen;
        private System.Windows.Forms.CheckBox cbCollege;
        private System.Windows.Forms.TextBox txtFarms;
        private System.Windows.Forms.TextBox txtCrime;
        private System.Windows.Forms.TextBox txtLabor;
        private System.Windows.Forms.TextBox txtPopDen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox cbInterstate;
        private System.Windows.Forms.CheckBox cbRecAreas;
        private System.Windows.Forms.TextBox txtInterstate;
        private System.Windows.Forms.TextBox txtRec;
    }
}