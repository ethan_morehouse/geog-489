﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;


namespace Lesson4_SiteSelection
{
    public class SiteSelection : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public SiteSelection()
        {
        }

        protected override void OnClick()
        {
            SiteSelectionMethods.pMxDoc = (IMxDocument)ArcMap.Application.Document;
            SiteSelectionMethods.pMap = SiteSelectionMethods.pMxDoc.FocusMap;

            if (SiteSelectionMethods.GetLayer("cities") == null)
            {
                MessageBox.Show("The 'cities' layer cannot be found.  Please add it to the map and try again.");
                return;
            }
            if (SiteSelectionMethods.GetLayer("counties") == null)
            {
                MessageBox.Show("The 'counties' layer cannot be found.  Please add it to the map and try again.");
                return;
            }

            

            Criteria form = new Criteria();
            form.ShowDialog();
        }

        protected override void OnUpdate()
        {
        }
    }
}
