﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.esriSystem;
using System.Diagnostics;

namespace Lesson4_SiteSelection
{
    class SiteSelectionMethods
    {
        public static IMxDocument pMxDoc;
        public static IMap pMap;

        public static void SiteSelection(string farmsSQL, string laborSQL, string crimeSQL, string popDensSQL, string collegeSQL, double interstate, double recAreas)
        {
            //create arrays for SQL Constructor
            string[] arrCountySQL = new string[] {farmsSQL, laborSQL, popDensSQL};
            string[] arrCitySQL = new string[] { crimeSQL, collegeSQL };
            
            //Construct SQL statements
            string CountySQL = ConstructSQL(arrCountySQL);           
            string CitySQL = ConstructSQL(arrCitySQL);

            //Point to counties layer
            IFeatureLayer pFLayer = GetFeatureLayer("counties");
            IFeatureClass pFClass = pFLayer.FeatureClass;

            //construct Query Filter
            IQueryFilter pQueryFilter = new QueryFilter();
            pQueryFilter.WhereClause = CountySQL;

            //Get Geometry from selected features
            IGeometry pGeomCounty = SelectedFeatureGeometry(pFClass, pQueryFilter);

            //point to cities
            pFLayer = GetFeatureLayer("cities");
            IFeatureSelection pFeatureSelection = (IFeatureSelection)pFLayer;

            //Construct Spatial Filter
            ISpatialFilter pSpatialFilter = new SpatialFilterClass();
            pSpatialFilter.Geometry = pGeomCounty;
            pSpatialFilter.GeometryField = "SHAPE";
            pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelContains;

            //construct Query Filter
            pQueryFilter.WhereClause = CitySQL;

            pFeatureSelection.SelectFeatures(pSpatialFilter, esriSelectionResultEnum.esriSelectionResultNew, false);
            pFeatureSelection.SelectFeatures(pQueryFilter, esriSelectionResultEnum.esriSelectionResultAnd, false);

            //apply recreation area spatial criteria
            if (recAreas != 0)
            {
                pSpatialFilter.Geometry = ProjectBufferGeometry("recareas", recAreas);
                pSpatialFilter.GeometryField = "SHAPE";
                pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelContains;

                pFeatureSelection.SelectFeatures(pSpatialFilter, esriSelectionResultEnum.esriSelectionResultAnd, false);
            }

            //apply interstate spatial criteria
            if (interstate != 0)
            {
                pSpatialFilter.Geometry = ProjectBufferGeometry("interstates", interstate);
                pSpatialFilter.GeometryField = "SHAPE";
                pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelContains;

                pFeatureSelection.SelectFeatures(pSpatialFilter, esriSelectionResultEnum.esriSelectionResultAnd, false);
            }
            pMxDoc.ActiveView.Refresh();
        }

        public static IGeometry ProjectBufferGeometry(string Name, double distance)
        {
            double bufferMeters = distance * 5280;

            IFeatureLayer pFLayer = GetFeatureLayer(Name);
            IFeatureClass pFClass = pFLayer.FeatureClass;
            IGeometry pGeometry = SelectedFeatureGeometry(pFClass, null);

            IGeometry projectedShape = (pGeometry as IClone).Clone() as IGeometry;
            ISpatialReferenceFactory3 srFactory = new SpatialReferenceEnvironmentClass();
            IProjectedCoordinateSystem pcs = srFactory.CreateProjectedCoordinateSystem((int)esriSRProjCS3Type.esriSRProjCS_NAD1983HARNSPCSPennsylvaniaNorthUSFt);
            pcs.SetFalseOriginAndUnits(-180, -90, 1000000);
            projectedShape.Project(pcs);
            IGeometry5 buf = (projectedShape as ITopologicalOperator).Buffer(bufferMeters) as IGeometry5;

            buf.Project5(pGeometry.SpatialReference, 0);

            return buf;
        }

        public static IGeometry SelectedFeatureGeometry(IFeatureClass pFClass, IQueryFilter pQueryFilter)
        {
            IEnumGeometry pEnumGeom;
            pEnumGeom = new EnumFeatureGeometry();

            IEnumGeometryBind pEnumGeomBind = default(IEnumGeometryBind);
            pEnumGeomBind = (IEnumGeometryBind)pEnumGeom;
            pEnumGeomBind.BindGeometrySource(pQueryFilter, pFClass);

            IGeometryFactory pGeomFactory;
            pGeomFactory = (IGeometryFactory)new GeometryEnvironment();

            IGeometry pGeom;
            pGeom = pGeomFactory.CreateGeometryFromEnumerator(pEnumGeom);

            return pGeom;
        }

        public static IFeatureLayer GetFeatureLayer(string name)
        {
            ILayer pLayer = GetLayer(name);
            IFeatureLayer pFLayer = (IFeatureLayer)pLayer;
            return pFLayer;
        }

        public static string ConstructSQL(string[] SQLStatements)
        {
            string concatenatedSQL = "";
            foreach (string sql in SQLStatements)
            {
                if (!string.IsNullOrEmpty(sql))
                {
                    if (string.IsNullOrEmpty(concatenatedSQL))
                    {
                        concatenatedSQL = sql;
                    }
                    else
                    {
                        concatenatedSQL += " AND " + sql;
                    }
                }
            }
            return concatenatedSQL;
        }

        public static ILayer GetLayer(string name)
        {
            IEnumLayer pLayers = pMap.Layers;
            pLayers.Reset();
            ILayer pLayer = pLayers.Next();
            while(pLayer != null) 
            {
                if (pLayer.Name == name && pLayer is IFeatureLayer)
                {
                    return pLayer;
                }
                pLayer = pLayers.Next();
            }
            return null;
        }
    }
}
