﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;
using System.Diagnostics;

namespace Lesson4_SiteSelection
{
    public partial class Criteria : Form
    {
        public Criteria()
        {
            InitializeComponent();
        }

        #region"CheckedChange Events - Enable and Disable text boxes"
        private void cbFarms_CheckedChanged(object sender, EventArgs e)
        {
            txtFarms.Enabled = cbFarms.Checked;
        }

        private void cbLabor_CheckedChanged(object sender, EventArgs e)
        {
            txtLabor.Enabled = cbLabor.Checked;
        }

        private void cbCrime_CheckedChanged(object sender, EventArgs e)
        {
            txtCrime.Enabled = cbCrime.Checked;
        }

        private void cbPopDen_CheckedChanged(object sender, EventArgs e)
        {
            txtPopDen.Enabled = cbPopDen.Checked;
        }

        private void cbInterstate_CheckedChanged(object sender, EventArgs e)
        {
            txtInterstate.Enabled = cbInterstate.Checked;
        }

        private void cbRecAreas_CheckedChanged(object sender, EventArgs e)
        {
            txtRec.Enabled = cbRecAreas.Checked;
        }
        #endregion

        #region"Restrict Textbox values to numbers only"
        private void txtboxkeypress (object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string farmsSQL = "";
            string laborSQL = "";
            string crimeSQL = "";
            string popDensSQL = "";
            string collegeSQL = "";
            double interstate = 0;
            double rec = 0;

            foreach (Control cb in this.Controls)
            {
                if (cb.GetType().ToString() == "System.Windows.Forms.CheckBox")
                {
                    switch (cb.Name)
                    {
                        case "cbFarms":                          
                            if ((cb as CheckBox).Checked && txtFarms.Text.Length > 0)
                            {
                                farmsSQL = "NO_FARMS87 > " + txtFarms.Text;
                            }
                            break;

                        case "cbLabor":                           
                            if ((cb as CheckBox).Checked && txtLabor.Text.Length > 0)
                            {
                                laborSQL = "AGE_18_64 >= " + txtLabor.Text;
                            }
                            break;

                        case "cbCrime":
                            if ((cb as CheckBox).Checked && txtCrime.Text.Length > 0)
                            {
                                crimeSQL = "CRIME_INDE <= " + txtCrime.Text;
                            }
                            break;

                        case "cbPopDen":                           
                            if ((cb as CheckBox).Checked && txtPopDen.Text.Length > 0)
                            {
                                popDensSQL = "POP_SQMILE < " + txtPopDen.Text;
                            }
                            break;

                        case "cbCollege":
                            if ((cb as CheckBox).Checked)
                            {
                                collegeSQL = "UNIVERSITY = 1";
                            }
                            else
                            {
                                collegeSQL = "UNIVERSITY = 0";
                            }
                            break;
                        case "cbInterstate":
                            if ((cb as CheckBox).Checked && txtInterstate.Text.Length > 0)
                            {
                                interstate = Convert.ToDouble(txtInterstate.Text);
                            }
                            break;
                        case "cbRecAreas":
                            if ((cb as CheckBox).Checked && txtRec.Text.Length > 0)
                            {
                                rec = Convert.ToDouble(txtRec.Text);
                            }
                            break;
                    }
                }
            }

            SiteSelectionMethods.SiteSelection(farmsSQL, laborSQL, crimeSQL, popDensSQL, collegeSQL, interstate, rec);

            this.Close();

        }

        




    }
}
