﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geometry;



namespace Lesson4PracticeExercises
{
    public class Exercise4 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Exercise4()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        public void procedure()
        {
            try
            {
                IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
                IMap pMap = pMxDoc.FocusMap;

                //get us_boundaries layer
                ILayer pLayerStates = getLayer("us_boundaries");

                //does not exist - quit
                if (pLayerStates == null)
                {
                    MessageBox.Show("'us_boundaries' does not exist in the current data frame");
                    return;
                }

                //get NY Geometry
                IFeatureLayer pFLayerStates = (IFeatureLayer)pLayerStates;
                IFeatureClass pFClassStates = pFLayerStates.FeatureClass;
                IQueryFilter pQueryFilter = new QueryFilter();
                pQueryFilter.WhereClause = "NAME = 'New York'";
                IFeatureCursor pfCursor = pFClassStates.Search(pQueryFilter, true);
                IFeature pFeature = pfCursor.NextFeature();
                IGeometry pNYGeometry = pFeature.Shape;


                //get us_roads layer
                ILayer pLayerRoads = getLayer("us_roads");
                
                //does not exist - quit
                if (pLayerRoads == null)
                {
                    MessageBox.Show("'us_roads' does not exist in the current data frame");
                    return;
                }

                IFeatureLayer pFLayerRoads = (IFeatureLayer)pLayerRoads;

                //create spatialFilter
                ISpatialFilter pSpatialFilter = new SpatialFilterClass();
                pSpatialFilter.Geometry = pNYGeometry;
                pSpatialFilter.GeometryField = "SHAPE";
                pSpatialFilter.SpatialRel = esriSpatialRelEnum.esriSpatialRelIntersects;

                //FeatureSelection = SpatialFilter
                IFeatureSelection pFeatureSelection = (IFeatureSelection)pFLayerRoads;
                pFeatureSelection.SelectFeatures(pSpatialFilter, esriSelectionResultEnum.esriSelectionResultNew, false);

                //update view
                pMxDoc.ActiveView.Refresh();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //Find and return ILayers
        public ILayer getLayer(string name)
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap pMap = pMxDoc.FocusMap;

            for (int i = 0; i < pMap.LayerCount; i++)
            {
                ILayer pLayer = pMap.Layer[i];
                if (pLayer.Name == name)
                {
                    return pLayer;
                }
            }

            return null;
        }
    }
}
