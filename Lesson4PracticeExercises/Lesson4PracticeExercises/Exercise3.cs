﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using System.Windows.Forms;


namespace Lesson4PracticeExercises
{
    public class Exercise3 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Exercise3()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        public void procedure() 
        {
            try
            {
                IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
                IMap pMap = pMxDoc.FocusMap;
                ILayer pLayer;

                bool layerFound = false;
                int i = 0;

                //get layer
                for (i = 0; i < pMap.LayerCount; i++)
                {
                    pLayer = pMap.Layer[i];
                    if (pLayer.Name == "us_cities" && pLayer is IFeatureLayer)
                    {
                        layerFound = true;
                        break;
                    }
                }

                //layer does not exist
                if (!layerFound)
                {
                    MessageBox.Show("Could not locate the 'us_cities' layer");
                    return;
                }

                IFeatureLayer pFLayer = (IFeatureLayer)pMap.Layer[i];

                //getFeatureClass
                IFeatureClass pFClass = pFLayer.FeatureClass;

                //Create QueryFilter
                IQueryFilter pQueryFilter = new QueryFilter();
                pQueryFilter.WhereClause = "POPCLASS = 4 or POPCLASS = 5";

                //create cursor
                IFeatureCursor pFCursor = pFClass.Search(pQueryFilter, true);

                int nameIndex = pFClass.FindField("Name");
                string cities = "The following cities are considered class 4 or 5 cities:";

                //cycle through features returned by cursor
                IFeature pFeature = pFCursor.NextFeature();
                while (pFeature != null)
                {
                    cities += "\n" + pFeature.Value[nameIndex];

                    pFeature = pFCursor.NextFeature();
                }

                //print cities
                MessageBox.Show(cities);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
