﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;


namespace Lesson4PracticeExercises
{
    public class Exercise2 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Exercise2()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        public void procedure()
        {
            //get table collection
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap pMap = pMxDoc.FocusMap;
            ITableCollection pTableCollection = (ITableCollection)pMap;

            ITable pTable;
            IDataset pDataset;

            bool TableFound = false;
            int i = 0;

            //cycle through tables
            for(i = 0; i < pTableCollection.TableCount; i++)
            {
                pTable = pTableCollection.Table[i];
                
                //to use Name attribute
                pDataset = (IDataset)pTable;

                //Find Table: break.
                if (pDataset.Name == "lu_codes")
                {
	                TableFound = true;
	                break;
                }
            }

            //table not found stop!
            if(!TableFound)
            {
                return;
            }

            //get table from location
            ITable pnTable = pTableCollection.Table[i];

            //get field indices
            int codesIndex = pnTable.FindField("LU_Code");
            int descIndex = pnTable.FindField("LU_Desc");

            //add rows
            TableEntry(pnTable, codesIndex, "RES", descIndex, "Residential");
            TableEntry(pnTable, codesIndex, "COM", descIndex, "Commercial");
            TableEntry(pnTable, codesIndex, "IND", descIndex, "Industrial");

        }

        //create rows and add data
        public void TableEntry(ITable pTable, int index1, string value1, int index2, string value2)
        {
            IRow row = pTable.CreateRow();
            row.set_Value(index1, value1);
            row.set_Value(index2, value2);
            row.Store();
        }
    }
}
