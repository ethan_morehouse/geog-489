﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.DataSourcesFile;
using ESRI.ArcGIS.SystemUI;
using System.Diagnostics;


namespace Lesson4PracticeExercises
{
    public class Exercise1 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public Exercise1()
        {
        }

        protected override void OnClick()
        {
            procedure();
        }

        protected override void OnUpdate()
        {
        }

        public void procedure() 
        {
            //Get focus maps and switch to "Source" view
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            pMxDoc.CurrentContentsView = pMxDoc.ContentsView[1];
            IMap pMap = pMxDoc.FocusMap;

            //create workspace
            IWorkspaceFactory pWSFactory = new ShapefileWorkspaceFactory();

            IFeatureWorkspace pFWorkspace = (IFeatureWorkspace)pWSFactory.OpenFromFile("c:/temp", ArcMap.Application.hWnd);

            //create fieldsEdit
            IFieldsEdit pFieldsEdit = (IFieldsEdit)new Fields();

            //create OID Field
            IFieldEdit pIDField = (IFieldEdit)new Field();
            pIDField.Name_2 = "OID";
            pIDField.Type_2 = esriFieldType.esriFieldTypeOID;
            pIDField.Length_2 = 8;

            //create lu_code field
            IFieldEdit pCodeField = (IFieldEdit)new Field();
            pCodeField.Name_2 = "LU_Code";
            pCodeField.Type_2 = esriFieldType.esriFieldTypeString;
            pCodeField.Length_2 = 4;

            //create lu_desc field
            IFieldEdit pDescField = (IFieldEdit)new Field();
            pDescField.Name_2 = "LU_Desc";
            pDescField.Type_2 = esriFieldType.esriFieldTypeString;
            pDescField.Length_2 = 25;

            //add fields
            pFieldsEdit.AddField(pIDField);
            pFieldsEdit.AddField(pCodeField);
            pFieldsEdit.AddField(pDescField);

            //create table and pass fields
            ITable pTable;
            if (File.Exists("C:/temp/lu_codes.dbf")) { File.Delete("C:/temp/lu_codes.dbf"); }
            pTable = pFWorkspace.CreateTable("lu_codes.dbf", pFieldsEdit, null, null, "");

            //add table to map
            ITableCollection pTableCollection = default(ITableCollection);
            pTableCollection = (ITableCollection)pMap;

            pTableCollection.AddTable(pTable);
            pMxDoc.UpdateContents();

        }
    }
}
