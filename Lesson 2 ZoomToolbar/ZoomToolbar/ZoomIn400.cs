﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ZoomToolbar
{
    public class ZoomIn400 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public ZoomIn400()
        {
        }

        protected override void OnClick()
        {
            Zoom.ChangeZoom(.25, .25);
        }
        protected override void OnUpdate()
        {
            Enabled = ArcMap.Application != null;
        }
    }

}
