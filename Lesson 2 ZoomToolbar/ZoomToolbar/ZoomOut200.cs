﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace ZoomToolbar
{
    public class ZoomOut200 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public ZoomOut200()
        {
        }

        protected override void OnClick()
        {
            Zoom.ChangeZoom(2.0, 2.0);
        }

        protected override void OnUpdate()
        {
        }
    }
}
