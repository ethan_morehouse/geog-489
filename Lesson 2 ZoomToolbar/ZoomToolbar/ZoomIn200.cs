﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace ZoomToolbar
{
    public class ZoomIn200 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public ZoomIn200()
        {
        }

        protected override void OnClick()
        {
            Zoom.ChangeZoom(.5, .5);
        }

        protected override void OnUpdate()
        {
        }
    }
}
