﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;

namespace ZoomToolbar
{
    class Zoom
    {
        public static void ChangeZoom(double dx, double dy)
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IActiveView pActiveView = pMxDoc.ActiveView;
            IEnvelope pEnv = pActiveView.Extent;
            pEnv.Expand(dx, dy, true);
            pActiveView.Extent = pEnv;
            pActiveView.Refresh();
        }
    }
}
