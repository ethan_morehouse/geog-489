﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace ZoomToolbar
{
    public class ZoomOut400 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public ZoomOut400()
        {
        }

        protected override void OnClick()
        {
            Zoom.ChangeZoom(4.0, 4.0);
        }

        protected override void OnUpdate()
        {
        }
    }
}
