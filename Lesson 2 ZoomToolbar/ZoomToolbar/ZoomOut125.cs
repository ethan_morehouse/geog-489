﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace ZoomToolbar
{
    public class ZoomOut125 : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public ZoomOut125()
        {
        }

        protected override void OnClick()
        {
            Zoom.ChangeZoom(0.8, 0.8);
        }

        protected override void OnUpdate()
        {
        }
    }
}