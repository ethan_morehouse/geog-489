﻿namespace TermProject
{
    partial class SelectableVisibleLayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.selectabletab = new System.Windows.Forms.TabPage();
            this.btnSelectionInvert = new System.Windows.Forms.Button();
            this.btnSelectionNone = new System.Windows.Forms.Button();
            this.btnSelectionAll = new System.Windows.Forms.Button();
            this.clbSelection = new System.Windows.Forms.CheckedListBox();
            this.visibletab = new System.Windows.Forms.TabPage();
            this.btnVisibleInvert = new System.Windows.Forms.Button();
            this.btnVisibleNone = new System.Windows.Forms.Button();
            this.btnVisibleAll = new System.Windows.Forms.Button();
            this.cblVisible = new System.Windows.Forms.CheckedListBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.selectabletab.SuspendLayout();
            this.visibletab.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.selectabletab);
            this.tabControl1.Controls.Add(this.visibletab);
            this.tabControl1.Location = new System.Drawing.Point(8, 7);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(290, 355);
            this.tabControl1.TabIndex = 0;
            // 
            // selectabletab
            // 
            this.selectabletab.Controls.Add(this.btnSelectionInvert);
            this.selectabletab.Controls.Add(this.btnSelectionNone);
            this.selectabletab.Controls.Add(this.btnSelectionAll);
            this.selectabletab.Controls.Add(this.clbSelection);
            this.selectabletab.Location = new System.Drawing.Point(4, 22);
            this.selectabletab.Name = "selectabletab";
            this.selectabletab.Padding = new System.Windows.Forms.Padding(3);
            this.selectabletab.Size = new System.Drawing.Size(282, 329);
            this.selectabletab.TabIndex = 0;
            this.selectabletab.Text = "Selectable Layers";
            this.selectabletab.UseVisualStyleBackColor = true;
            // 
            // btnSelectionInvert
            // 
            this.btnSelectionInvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectionInvert.Location = new System.Drawing.Point(168, 16);
            this.btnSelectionInvert.Name = "btnSelectionInvert";
            this.btnSelectionInvert.Size = new System.Drawing.Size(100, 23);
            this.btnSelectionInvert.TabIndex = 3;
            this.btnSelectionInvert.Text = "Invert Selection";
            this.btnSelectionInvert.UseVisualStyleBackColor = true;
            this.btnSelectionInvert.Click += new System.EventHandler(this.btnSelectionInvert_Click);
            // 
            // btnSelectionNone
            // 
            this.btnSelectionNone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSelectionNone.Location = new System.Drawing.Point(91, 16);
            this.btnSelectionNone.Name = "btnSelectionNone";
            this.btnSelectionNone.Size = new System.Drawing.Size(75, 23);
            this.btnSelectionNone.TabIndex = 2;
            this.btnSelectionNone.Text = "Deselect All";
            this.btnSelectionNone.UseVisualStyleBackColor = true;
            this.btnSelectionNone.Click += new System.EventHandler(this.btnSelectionNone_Click);
            // 
            // btnSelectionAll
            // 
            this.btnSelectionAll.Location = new System.Drawing.Point(14, 16);
            this.btnSelectionAll.Name = "btnSelectionAll";
            this.btnSelectionAll.Size = new System.Drawing.Size(75, 23);
            this.btnSelectionAll.TabIndex = 1;
            this.btnSelectionAll.Text = "Select All";
            this.btnSelectionAll.UseVisualStyleBackColor = true;
            this.btnSelectionAll.Click += new System.EventHandler(this.btnSelectionAll_Click);
            // 
            // clbSelection
            // 
            this.clbSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.clbSelection.CheckOnClick = true;
            this.clbSelection.FormattingEnabled = true;
            this.clbSelection.Location = new System.Drawing.Point(14, 45);
            this.clbSelection.Name = "clbSelection";
            this.clbSelection.Size = new System.Drawing.Size(250, 274);
            this.clbSelection.TabIndex = 0;
            // 
            // visibletab
            // 
            this.visibletab.Controls.Add(this.btnVisibleInvert);
            this.visibletab.Controls.Add(this.btnVisibleNone);
            this.visibletab.Controls.Add(this.btnVisibleAll);
            this.visibletab.Controls.Add(this.cblVisible);
            this.visibletab.Location = new System.Drawing.Point(4, 22);
            this.visibletab.Name = "visibletab";
            this.visibletab.Padding = new System.Windows.Forms.Padding(3);
            this.visibletab.Size = new System.Drawing.Size(282, 329);
            this.visibletab.TabIndex = 1;
            this.visibletab.Text = "Visible Layers";
            this.visibletab.UseVisualStyleBackColor = true;
            // 
            // btnVisibleInvert
            // 
            this.btnVisibleInvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVisibleInvert.Location = new System.Drawing.Point(168, 16);
            this.btnVisibleInvert.Name = "btnVisibleInvert";
            this.btnVisibleInvert.Size = new System.Drawing.Size(100, 23);
            this.btnVisibleInvert.TabIndex = 9;
            this.btnVisibleInvert.Text = "Invert Selection";
            this.btnVisibleInvert.UseVisualStyleBackColor = true;
            this.btnVisibleInvert.Click += new System.EventHandler(this.btnVisibleInvert_Click);
            // 
            // btnVisibleNone
            // 
            this.btnVisibleNone.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnVisibleNone.Location = new System.Drawing.Point(91, 16);
            this.btnVisibleNone.Name = "btnVisibleNone";
            this.btnVisibleNone.Size = new System.Drawing.Size(75, 23);
            this.btnVisibleNone.TabIndex = 8;
            this.btnVisibleNone.Text = "Deselect All";
            this.btnVisibleNone.UseVisualStyleBackColor = true;
            this.btnVisibleNone.Click += new System.EventHandler(this.btnVisibleNone_Click);
            // 
            // btnVisibleAll
            // 
            this.btnVisibleAll.Location = new System.Drawing.Point(14, 16);
            this.btnVisibleAll.Name = "btnVisibleAll";
            this.btnVisibleAll.Size = new System.Drawing.Size(75, 23);
            this.btnVisibleAll.TabIndex = 7;
            this.btnVisibleAll.Text = "Select All";
            this.btnVisibleAll.UseVisualStyleBackColor = true;
            this.btnVisibleAll.Click += new System.EventHandler(this.btnVisibleAll_Click);
            // 
            // cblVisible
            // 
            this.cblVisible.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cblVisible.CheckOnClick = true;
            this.cblVisible.FormattingEnabled = true;
            this.cblVisible.Location = new System.Drawing.Point(14, 45);
            this.cblVisible.Name = "cblVisible";
            this.cblVisible.Size = new System.Drawing.Size(250, 274);
            this.cblVisible.TabIndex = 6;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Location = new System.Drawing.Point(12, 378);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(105, 378);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // SelectableVisibleLayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 413);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectableVisibleLayerForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Selectable and Visible Layers";
            this.tabControl1.ResumeLayout(false);
            this.selectabletab.ResumeLayout(false);
            this.visibletab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage selectabletab;
        private System.Windows.Forms.Button btnSelectionInvert;
        private System.Windows.Forms.Button btnSelectionNone;
        private System.Windows.Forms.Button btnSelectionAll;
        public System.Windows.Forms.CheckedListBox clbSelection;
        private System.Windows.Forms.TabPage visibletab;
        private System.Windows.Forms.Button btnVisibleInvert;
        private System.Windows.Forms.Button btnVisibleNone;
        private System.Windows.Forms.Button btnVisibleAll;
        public System.Windows.Forms.CheckedListBox cblVisible;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}