﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Catalog;
using System.Diagnostics;


namespace TermProject
{
    public partial class FrequentLayerForm : Form
    {
        //system paths to csv and directory to store layers
        public string csvPath = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "ArcGIS"), "FrequentDataLayers.csv");
        public string tempCSVPath = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "ArcGIS"), "FrequentDataLayersTemp.csv");
        public string layerDir = Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "ArcGIS"), "FrequentLayers");
        public char[] delim = { ',' };

        public FrequentLayerForm()
        {
            InitializeComponent();

        }

        #region "Replace Layers Button: Adds all the layers in the current data frame to the list of 'frequently used layers', Overwrites existing csv, and adds layers to frequent layers directory"
        private void btnAddLayers_Click(object sender, EventArgs e)
        {
            this.clbLayers.Items.Clear();

            ClearAll();

            IMxDocument mxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap map = mxDoc.FocusMap;

            //Create new csv for storing the frequently used data's name and filepath
            FileStream fs = null;
            try
            {
                fs = new FileStream(csvPath, FileMode.Create);
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    fs = null;
                    //Iterate through layers
                    for (int i = 0; i < map.LayerCount; i++)
                    {

                        ILayer layer = map.get_Layer(i);

                        if (!this.clbLayers.Items.Contains(layer.Name))
                        {
                            //Concatenate the save directory and the layer name
                            string savePath = layerDir + "\\" + layer.Name + ".lyr";

                            //Add entry to csv
                            sw.WriteLine(layer.Name + "," + savePath);

                            //add Item to Check List Box.
                            this.clbLayers.Items.Add(layer.Name);

                            //save Layer
                            SaveToLayerFile(savePath, layerDir, layer);
                        }
                    }
                }
            }
            finally
            {
                if (fs != null)
                {
                    fs = null;
                }
            }
            addBtnEnabledCheck();

        }
        #endregion

        #region "Okay Button: Adds selected Layers into map"
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!File.Exists(csvPath))
            {
                return;
            }
            // no items to add
            if (this.clbLayers.CheckedItems.Count < 1)
            {
                return;
            }

            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IActiveView pActiveView = pMxDoc.ActiveView;
            IMap pMap = pMxDoc.FocusMap;

            //List of layers to add
            List<LayerObject> layerList = new List<LayerObject>();

            //open file and reader stream
            FileStream fs = null;
            try
            {
                fs = new FileStream(csvPath, FileMode.Open);
                using (StreamReader rs = new StreamReader(fs))
                {
                    fs = null;

                    //read csv line & start counter
                    string line = rs.ReadLine();
                    int index = 0;

                    while (!string.IsNullOrEmpty(line))
                    {
                        if (this.clbLayers.CheckedIndices.Contains(index))
                        {
                            bool found = false;
                            
                            //Does the layer already exist in the data frame?
                            for (int i = 0; i < pMap.LayerCount; i++)
                            {
                                ILayer pLayer = pMap.get_Layer(i);

                                //Layer already exists
                                if (pLayer.Name == line.Split(delim)[0])
                                {
                                    found = true;
                                    break;
                                }
                            }

                            //if layer doesn't exist in map add to list of layers to add to map
                            if (!found)
                            {
                                string[] layer = line.Split(delim);
                                layerList.Add(new LayerObject(layer[0], layer[1]));
                            }
                        }

                        //read next line & increment counter
                        line = rs.ReadLine();
                        index++;
                    }
                }
            }
            finally
            {
                if (fs != null)
                {
                    fs = null;
                }
            }

            //reverse order, cycle through layers and add them to map.
            layerList.Reverse();
            foreach (LayerObject layer in layerList) 
            {
                AddLayerToActiveView(pActiveView, layer.path);
            }
            
            this.Close();
        }
        #endregion

        #region "Cancel button: closes form"
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region "Clear All Button: Removes all items from check list box and deletes csv and layers (with all subfiles)"
        private void btnClearAll_Click(object sender, EventArgs e)
        {
            ClearAll();
        }
        #endregion

        #region "ClearAll: deletes directory and CSV file"
        private void ClearAll()
        {
            //clears out frequent layers

            //delete csv
            if (File.Exists(csvPath))
            {
                File.Delete(csvPath);
            }
            if (Directory.Exists(layerDir))
            {
                Directory.Delete(layerDir, true);
            }
            

            //clear checked list box items
            this.clbLayers.Items.Clear();

            addBtnEnabledCheck();
        }
        #endregion

        #region "Select all button: Select all items in check list box"
        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            AllOrNothing(true);
        }
        #endregion

        #region "Deselect all button: Deselect all items in check list box"
        private void btnDeselectAll_Click(object sender, EventArgs e)
        {
            AllOrNothing(false);
        }
        #endregion

        #region"Invert Selection button: Inverts selection of check list box"
        private void btnInvert_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.clbLayers.Items.Count; i++)
            {
                this.clbLayers.SetItemChecked(i, !this.clbLayers.GetItemChecked(i));
            }
        }
        #endregion

        #region "AllOrNothing: sets or deselects all items in CLB
        private void AllOrNothing(bool status)
        {
            for (int i = 0; i < this.clbLayers.Items.Count; i++)
            {
                this.clbLayers.SetItemChecked(i, status);
            }
        }
        #endregion

        #region "Remove the currently selected items in the list from the csv, layer directory, and the checkboxlist"
        private void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            //check to see if csv exits
            if(!File.Exists(csvPath)) 
            {
                return;
            }

            //check to see if all layers are selected
            if (this.clbLayers.CheckedItems.Count == this.clbLayers.Items.Count)
            {
                //delete csv and directory
                ClearAll();
                return;
            }

            //create file streams
            FileStream fs = null;
            FileStream tempStream = null;

            try
            {
                fs = new FileStream(csvPath, FileMode.Open);
                tempStream = new FileStream(tempCSVPath, FileMode.Create);
                using (StreamWriter ws = new StreamWriter(tempStream))
                using (StreamReader rs = new StreamReader(fs))
                {
                    fs = null;
                    tempStream = null;

                    //read first line and start counter
                    string line = rs.ReadLine();
                    int index = 0;

                    //loop through lines
                    while (!string.IsNullOrEmpty(line))
                    {
                        //if layer is not to be removed
                        if (!this.clbLayers.CheckedIndices.Contains(index))
                        {
                            ws.WriteLine(line);
                        }

                        //remove layer
                        else
                        {
                            this.clbLayers.Items.RemoveAt(index);
                            File.Delete(line.Split(delim)[1]);
                        }
                        
                        //read next line and increment index counter
                        line = rs.ReadLine();
                        index++;
                    }
                }
            }
            finally
            {
                if (fs != null || tempStream != null)
                {
                    fs = null;
                    tempStream = null;
                }
            }

            //delete old csv
            File.Delete(csvPath);

            //rename temp csv
            File.Move(tempCSVPath, csvPath);
        }

        #endregion

        #region "Append Button: Appends layers in current data frame to list of'frequently used layers'"
        private void btnAppend_Click(object sender, EventArgs e)
        {
            
            IMxDocument mxDoc = (IMxDocument)ArcMap.Application.Document;
            IMap map = mxDoc.FocusMap;
            
            //open file and writer stream
            FileStream fs = new FileStream(csvPath, FileMode.Append);
            StreamWriter sw = new StreamWriter(fs);

            //Iterate over layers
            try
            {
                for(int i = 0; i < map.LayerCount; i++)
                {
                    //only append if layer doesn't exist.  Don't want duplicate layers.
                    if (!this.clbLayers.Items.Contains(map.get_Layer(i).Name))
                    {
                        ILayer pLayer = map.get_Layer(i);

                        //Concatenate the save directory and the layer name
                        string savePath = layerDir + "\\" + pLayer.Name + ".lyr";

                        //Add entry to csv
                        sw.WriteLine(pLayer.Name + "," + savePath);

                        //add Item to Check List Box.
                        this.clbLayers.Items.Add(pLayer.Name);

                        //save Layer
                        SaveToLayerFile(savePath, layerDir, pLayer);
                    }
                }
            }
            finally 
            {
                //close streams
                sw.Close();
                fs.Close();
            }

            //check to see if the button to add layers to the map should be enabled
            addBtnEnabledCheck();
        }
        #endregion

        #region "Snippet: Save Layer To File"

        ///<summary>Write and Layer to a file on disk.</summary>
        ///  
        ///<param name="layerFilePath">A System.String that is the path and filename for the layer file to be created. Example: "C:\temp\cities.lyr"</param>
        ///<param name="layer">An ILayer interface.</param>
        ///   
        ///<remarks></remarks>
        public void SaveToLayerFile(String layerFilePath, string dirPath, ILayer layer)
        {
            if(layer == null)
            {
                return;
            }
            //create a new LayerFile instance
            ILayerFile layerFile = new LayerFileClass();

            //make sure that the layer file name is valid
            if (System.IO.Path.GetExtension(layerFilePath) != ".lyr")
                return;
            if (!Directory.Exists(dirPath)) 
            {
                Directory.CreateDirectory(dirPath);
            }
            if (layerFile.get_IsPresent(layerFilePath))
                    System.IO.File.Delete(layerFilePath);

            //create a new layer file
            layerFile.New(layerFilePath);

            //attach the layer file with the actual layer
            layerFile.ReplaceContents(layer);

            //save the layer file
            layerFile.Save();
        }
        #endregion

        #region "Snippet: Add Layer File to ActiveView"

        ///<summary>Add a layer file (.lyr) into the active view when the path (on disk or network) is specified.</summary>
        ///      
        ///<param name="activeView">An IActiveview interface</param>
        ///<param name="layerPathFile">A System.String that is the path\filename of a layer file. Example: "C:\temp\mylayer.lyr".</param>
        ///      
        ///<remarks></remarks>
        public void AddLayerToActiveView(IActiveView activeView, String layerPathFile)
        {

            if (activeView == null || layerPathFile == null || !layerPathFile.EndsWith(".lyr"))
            {
                return;
            }
  
            // Create a new GxLayer
            IGxLayer gxLayer = new GxLayerClass();

            IGxFile gxFile = (IGxFile)gxLayer; //Explicit Cast

            // Set the path for where the layerfile is located on disk
            gxFile.Path = layerPathFile;

            // Test if we have a valid layer and add it to the map
            if (!(gxLayer.Layer == null))
            {
                IMap map = activeView.FocusMap;
                map.AddLayer(gxLayer.Layer);
            }
        }
        #endregion

        #region "Tooltips"
        private void btnAppend_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Appends all the layers within the current data frame to the list of 'frequently used layers'";
        }

        private void btnSelectAll_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "checks all items in list";
        }

        private void btnDeselectAll_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Unchecks all items in list";
        }

        private void btnCancel_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Closes this window without adding any layers";
        }

        private void btnAdd_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Adds the currently checked items to the current data frame";
        }

        private void btnClearAll_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Deletes all the previously saved 'frequently used layers'. This action cannot be undone.";
        }

        private void btnRemoveSelected_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Removes the currently selected layers from the list of 'frequently used layers'.  This action cannot be undone.";
        }

        private void btnAddLayers_MouseEnter(object sender, EventArgs e)
        {
            this.textBox1.Text = "Replaces all the previously saved 'frequently used layers' with the layers in the active data frame. This action cannot be undone.";
        }

        private void TooltipRefresh(object sender, EventArgs e)
        {
            if (this.clbLayers.Items.Count < 1)
            {
                this.textBox1.Text = "You must add layers to the list before you can add them to your map.  Close this window and proceed to add layers to your map then reopen this window and click 'Replace Layers'";
            }
            else
            {
                this.textBox1.Text = "Tooltip:";
            }
        }
        #endregion

        #region "addBtnEnabledCheck(): checks to see if "Okay" button should be enabled"
        public void addBtnEnabledCheck()
        {
            if (this.clbLayers.Items.Count > 0)
            {
                this.btnAdd.Enabled = true;
            }
            else
            {
                this.btnAdd.Enabled = false;
                this.textBox1.Text = "You must add layers to the list before you can add them to your map.  Close this window and proceed to add layers to your map then reopen this window and click 'Replace Layers'";
            }
        }
        #endregion
    }
}
