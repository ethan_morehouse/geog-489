﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using ESRI.ArcGIS.Catalog;


namespace TermProject
{
    class RasterMethods
    {
        public static string ArcGISFolder = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "ArcGIS");
        public static string CsvSavePath = System.IO.Path.Combine(ArcGISFolder, "orthos.csv");
        public static string TempCsvPath = System.IO.Path.Combine(ArcGISFolder, "Temp.csv");
        public static string FolderLocation = System.IO.Path.Combine(ArcGISFolder, "orthos");
        public static char delim = ',';
        public static IMxDocument pMxDoc;
        public static IMap pMap;
        public static string m_Category;

        //Child Process: Adds rasters to the saved list
        public static List<ILayer> LoadRastersIntoSavedList()
        {
            //create List<IRasterLayer>
            List<ILayer> layers = new List<ILayer>();

            //cycle through all layers
            IEnumLayer pLayers = pMap.get_Layers(); ;
            ILayer pLayer = pLayers.Next();
            while (pLayer != null)
            {
                //append only RasterLayers
                if (pLayer is IRasterLayer)
                {
                    layers.Add(pLayer);
                }
                pLayer = pLayers.Next();
            }

            //return List<IRasterLayer>
            return layers;
        }

        //Parent Process: Reads csv file.  returns string list unique entries of index.  (Category, Layername, Filepath)
        public static List<string> ReadCsv(int index)
        {
            //Check if file exists
            if (!File.Exists(CsvSavePath))
            {
                return null;
            }

            //Create List type string to return
            List<string> returnList = new List<string>();

            //open file stream
            FileStream fs = null;
            try
            {
                fs = new FileStream(CsvSavePath, FileMode.Open);
                using (StreamReader sr = new StreamReader(fs))
                {
                    fs = null;

                    //read lines one at a time
                    string line = sr.ReadLine();
                    while (!string.IsNullOrEmpty(line))
                    {

                        //split lines
                        string entry = line.Split(delim)[index];

                        //only add unique entries
                        if (!returnList.Contains(entry))
                        {
                            returnList.Add(entry);
                        }

                        line = sr.ReadLine();
                    }
                }
            }
            finally
            {
                if (fs != null)
                {
                    fs = null;
                }
            }

            //return List of string[]'s
            return returnList;
        }

        //Sub-Child Process: Save Layer to File and return Layer's save name
        public static string SaveToLayerFile(ILayer layer)
        {

            if (layer == null)
            {
                return null;
            }

            string layerFilePath = getLayerPath(layer);

            //create a new LayerFile instance
            ILayerFile layerFile = new LayerFileClass();

            //check if folder exists, if not create it.
            if (!Directory.Exists(FolderLocation))
            {
                Directory.CreateDirectory(FolderLocation);
            }

            //make sure that the layer file name is valid
            if (System.IO.Path.GetExtension(layerFilePath) != ".lyr")
            {
                return null;
            }
            if (layerFile.get_IsPresent(layerFilePath))
            {
                System.IO.File.Delete(layerFilePath);
            }

            //create a new layer file
            layerFile.New(layerFilePath);

            //attach the layer file with the actual layer
            layerFile.ReplaceContents(layer);

            //save the layer file
            layerFile.Save();

            return layerFilePath;
        }

        //Sub-Child Process: Returns valid layer file path
        public static string getLayerPath(ILayer layer)
        {
            string pathAttempt = System.IO.Path.Combine(FolderLocation, layer.Name + ".lyr");

            //test to see if filename is used
            if (File.Exists(System.IO.Path.Combine(FolderLocation, layer.Name + ".lyr")))
            {
                //append number on end until layer filename is valid.
                int j = 0;
                while (File.Exists(System.IO.Path.Combine(FolderLocation, layer.Name + j.ToString() + ".lyr")))
                {
                    j++;
                }
                pathAttempt = System.IO.Path.Combine(FolderLocation, layer.Name + j.ToString() + ".lyr");
            }

            //return filename
            return pathAttempt;
        }

        //Sub-Child Process: Returns an group layer named by the category being added - can create new group layer if second parameter is true.
        public static IGroupLayer GetGroupLayer(IActiveView activeView)
        {
            //loop through layers
            for (int i = 0; i < pMap.LayerCount; i++)
            {
                ILayer pLayer = pMap.get_Layer(i);

                //return group layer
                if (pLayer is IGroupLayer && pLayer.Name == m_Category)
                {
                    IGroupLayer pGLayer = (IGroupLayer)pLayer;
                    return pGLayer;
                }
            }

            return null;
        }

        //Parent Process: Deletes all entries which match the specified category. Creates temp csv to save entries that
        //will not be deleted.  Will then delete original csv and rename temp.
        public static void DeleteCategory(string category)
        {
            //open file streams
            FileStream fs = null;
            FileStream ts = null;

            try
            {
                fs = new FileStream(CsvSavePath, FileMode.Open);
                ts = new FileStream(TempCsvPath, FileMode.Create);
                using (StreamWriter sw = new StreamWriter(ts))
                using (StreamReader sr = new StreamReader(fs))
                {
                    fs = null;
                    ts = null;

                    //cycle through lines
                    string line = sr.ReadLine();
                    while (!String.IsNullOrEmpty(line))
                    {

                        //only copy over entries not in deleted category
                        if (line.Split(delim)[0] != category)
                        {
                            sw.WriteLine(line);
                        }

                        //don't copy over and delete file.
                        else
                        {
                            DeleteLayerFile(line.Split(delim)[2]);
                        }
                        line = sr.ReadLine();
                    }
                }
            }
            finally
            {
                if (fs != null || ts != null)
                {
                    fs = null;
                    ts = null;
                }
            }

            //delete Orginal Csv and Rename temp.
            File.Delete(CsvSavePath);
            File.Move(TempCsvPath, CsvSavePath);

            if (IsDirectoryEmpty())
            {
                Directory.Delete(FolderLocation);
                File.Delete(CsvSavePath);
            }

        }

        public static bool IsDirectoryEmpty()
        {
            string[] files = Directory.GetFiles(FolderLocation);
            return files.Count<string>() == 0;
        }

        //Child Process: deletes the file at the specified filepath
        public static void DeleteLayerFile(string filepath)
        {
            if (!File.Exists(filepath))
            {
                return;
            }

            File.Delete(filepath);
        }

        //Parent Process: Add all raster layers in the map to an existing category.
        public static void AddToExistingCategory(string category)
        {

            //get List of Layer information
            List<ILayer> LayerList = LoadRastersIntoSavedList();

            //append non duplicate layers to csv.
            AddEntriesToCsv(LayerList, category);
        }

        //Child Process: Appends Entries to CSV
        public static void AddEntriesToCsv(List<ILayer> LayerList, string category)
        {
            //open file stream
            FileStream fs = null;

            try
            {
                fs = new FileStream(CsvSavePath, FileMode.Append);
                using (StreamWriter sw = new StreamWriter(fs))
                {

                    //cycle through ILayers
                    foreach (ILayer layer in LayerList)
                    {

                        //save layer to file and get unique filename + path
                        string savepath = SaveToLayerFile(layer);

                        //write new entry
                        sw.WriteLine(String.Format("{0},{1},{2}", category, layer.Name, savepath));
                    }
                }
            }
            finally
            {
                if (fs != null)
                {
                    fs = null;
                }
            }
        }

        //Parent Process: Add all rasters in the map to a new category
        public static void CreateCategory(string category)
        {

            //get ILayer list of all Rasters to save.
            List<ILayer> LayerList = LoadRastersIntoSavedList();

            //Add all ILayers in the list to CSV with the following category
            AddEntriesToCsv(LayerList, category);
        }

        //Child Process: Reads orthos csv file.  Creates string list of all entries matching the category and returns only
        //the item specified. (Category, Layer Name, Filepath)
        public static List<String> ReadCsvCategory(String category, int index)
        {
            //Create List of LayerPaths to return
            List<String> LayerPaths = new List<String>();

            //Get List of rasters that already exist in the "category" group layer -- Don't want dups!
            List<string> CurrentlyAddedRasters = ExistingRasters();

            //open file stream
            FileStream fs = null;
            try
            {
                fs = new FileStream(CsvSavePath, FileMode.Open);
                using (StreamReader sr = new StreamReader(fs))
                {
                    fs = null;

                    //cycle through lines.
                    string line = sr.ReadLine();
                    while (!String.IsNullOrEmpty(line))
                    {

                        //split
                        string[] entry = line.Split(delim);

                        //only add raster layer to LayerPaths if it is in the category selected and is not already in the category's group layer.
                        if (entry[0] == category && !CurrentlyAddedRasters.Contains(entry[1]))
                        {
                            LayerPaths.Add(entry[index]);
                        }

                        line = sr.ReadLine();
                    }
                }
            }
            finally
            {
                if (fs != null)
                {
                    fs = null;
                }
            }

            //return list of ILayers to add to the group layer.
            return LayerPaths;
        }

        //Sub-Child Process: Returns a list of existing IRasterLayer's in the current map within the specified group layer
        public static List<string> ExistingRasters()
        {
            List<string> RasterList = new List<string>();

            //get category's group layer
            IGroupLayer pGroupLayer = GetGroupLayer(pMxDoc.ActiveView);
            ICompositeLayer pCompositeLayer = (ICompositeLayer)pGroupLayer;

            //category has not been added to TOC yet, therefore add all rasters.
            if (pCompositeLayer == null)
            {
                return RasterList;
            }

            //cycle through items and add to RasterList
            for (int i = 0; i < pCompositeLayer.Count; i++)
            {
                RasterList.Add(pCompositeLayer.Layer[i].Name);
            }

            return RasterList;
        }

        //Child Process: Returns list of Ilayer Objects
        public static List<IRasterLayer> GetILayerList(List<string> layerPaths)
        {
            //create ILayer list
            List<IRasterLayer> ILayerList = new List<IRasterLayer>();

            foreach (string layerpath in layerPaths)
            {
                // Create a new GxLayer
                IGxLayer gxLayer = new GxLayerClass();

                IGxFile gxFile = (IGxFile)gxLayer; //Explicit Cast

                // Set the path for where the layerfile is located on disk
                gxFile.Path = layerpath;

                // Test if we have a valid layer and add it to the map
                if (!(gxLayer.Layer == null))
                {
                    ILayer pLayer = gxLayer.Layer;
                    IRasterLayer pRasterLayer = (IRasterLayer)pLayer;
                    ILayerList.Add(pRasterLayer);
                }
            }

            return ILayerList;
        }

        //Child Process: Accepts a list of IRasterLayers and looks to see if it intersects with or is contained by the Active View.
        public static void CheckForIntersection(List<IRasterLayer> RasterList)
        {

            //create list of IRasterLayers that will be added
            List<IRasterLayer> IntersectingLayers = new List<IRasterLayer>();

            IActiveView pActiveView = pMxDoc.ActiveView;

            //get extent of view
            IEnvelope mapExtent = pActiveView.Extent;

            //Create IRelationalOperator from envelope (Used to test for overlap).
            IRelationalOperator pRelOperator = (IRelationalOperator)mapExtent;

            //loop through IRasterLayers
            foreach (IRasterLayer pRLayer in RasterList)
            {

                //get raster extent
                IEnvelope pRasterEnvelope = pRLayer.VisibleExtent;

                //project the rasters envelope to match the current extent
                pRasterEnvelope.Project(mapExtent.SpatialReference);

                //make sure spatial reference is the same
                if (mapExtent.SpatialReference.FactoryCode == pRasterEnvelope.SpatialReference.FactoryCode)
                {

                    if (pRelOperator.Overlaps(pRasterEnvelope) || pRelOperator.Contains(pRasterEnvelope))
                    {
                        //Add layer to a list of layers to add to the map.
                        IntersectingLayers.Add(pRLayer);
                    }
                }
            }

            //If 1 or more layers intersect add them to the map.
            if (IntersectingLayers.Count > 0)
            {
                AddLayersToMap(IntersectingLayers, pActiveView);
            }
        }

        //Sub-Child Process: Adds IRasterLayers that Intersect the active view to the into the category's group layer.
        public static void AddLayersToMap(List<IRasterLayer> IntersectingLayers, IActiveView pActiveView)
        {
            //get group layer
            IGroupLayer groupLayer = GetGroupLayer(pActiveView);

            bool newGroup = false;

            if (groupLayer == null)
            {
                //Ortho Group layer does not exist. Create new one and add it to map
                newGroup = true;
                groupLayer = new GroupLayerClass();
                groupLayer.Name = m_Category;
            }

            //loop Rasters and add to group layer
            foreach (IRasterLayer pRLayer in IntersectingLayers)
            {
                groupLayer.Add(pRLayer);
            }

            //add group layer to map and update
            if (newGroup)
            {
                pMxDoc.AddLayer(groupLayer);
            }
            UpdateDocument();


        }

        //Child Process: Refresh the map document
        public static void UpdateDocument()
        {
            pMxDoc.ActiveView.Refresh(); ;
            pMxDoc.UpdateContents();
        }

        //Parent Process: Add Raster Layers to map that are in specified category.
        public static void AddRasterToMap(string category)
        {

            m_Category = category;

            //get List<String> of LayerPaths based upon Category
            List<String> ListFilePathList = ReadCsvCategory(category, 2);

            //Load Layers into List<IRasterLayer>
            List<IRasterLayer> ListIRLayer = GetILayerList(ListFilePathList);

            //pass to relationalOperator - add any rasters to the map that intersect or are contained
            //within the activeview extent
            CheckForIntersection(ListIRLayer);
        }
    }
}