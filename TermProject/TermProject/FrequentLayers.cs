﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.VisualBasic;


namespace TermProject
{
    public class FrequentLayers : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public FrequentLayers()
        {
        }

        protected override void OnClick()
        {
                        //create new form

            FrequentLayerForm form = new FrequentLayerForm();

            //check to see if csv exists
            if (!File.Exists(form.csvPath))
            {
                //nothing to load into checked list box: disable btnAdd and set tooltip to get started message.
                form.addBtnEnabledCheck();
                form.ShowDialog();
                return;
            }

            //csv delimiter
            char[] delim = { ',' };

            //create file stream
            FileStream fs = null;
            try
            {
                fs = new FileStream(form.csvPath, FileMode.Open);

                //create Reader
                using (StreamReader rs = new StreamReader(fs))
                {
                    fs = null;

                    string line = rs.ReadLine();

                    //read line by line
                    while (!String.IsNullOrEmpty(line))
                    {
                        string[] split = line.Split(delim, 2);
                        form.clbLayers.Items.Add(split[0]);

                        //next line
                        line = rs.ReadLine();
                    }
                }
            }
            finally
            {
                if (fs != null)
                    fs.Dispose();
            }

            form.btnAdd.Enabled = true;
            form.textBox1.Text = "Tooltip:";

            //show form
            form.ShowDialog(); ;
        }

        protected override void OnUpdate()
        {
        }
    }
}
