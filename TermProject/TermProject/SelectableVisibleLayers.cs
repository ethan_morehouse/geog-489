﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;


namespace TermProject
{
    public class SelectableVisibleLayers : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public SelectableVisibleLayers()
        {
        }

        protected override void OnClick()
        {
            InitializeForm();
        }

        protected override void OnUpdate()
        {
        }

        //create form
        private void InitializeForm()
        {
            //create new form
            SelectableVisibleLayerForm form = new SelectableVisibleLayerForm();
            PopulateForm(form);
            form.ShowDialog();
        }

        //populate Check List Boxes with items and set initial check state.
        private void PopulateForm(SelectableVisibleLayerForm form)
        {
            IEnumLayer pLayers = GetLayers();
            pLayers.Reset();
            ILayer pLayer = pLayers.Next();
            
            //keep track of Check List Box's Indices.
            int visibleindex = 0;
            int selectindex = 0;

            //cycle through layers
            while (pLayer != null)
            {
                // All ILayers have Visible attribute
                form.cblVisible.Items.Add(pLayer.Name);
                if (pLayer.Visible)
                {
                    form.cblVisible.SetItemChecked(visibleindex, true);
                }

                // Only Feature Layers have Selectable Attribute
                if (pLayer is IFeatureLayer2)
                {  
                    form.clbSelection.Items.Add(pLayer.Name);
                    IFeatureLayer2 pFeatureLayer = (IFeatureLayer2)pLayer;
                    if (pFeatureLayer.Selectable)
                    {
                        form.clbSelection.SetItemChecked(selectindex, true);
                    }

                    //Update Index
                    selectindex++;
                }
                //update index
                visibleindex++;

                //next layer
                pLayer = pLayers.Next();
            }
        }

        //quick access to IEnumLayer object
        public static IEnumLayer GetLayers()
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IEnumLayer pLayers = pMxDoc.FocusMap.get_Layers();
            return pLayers;
        }
    }
}
