﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;


namespace TermProject
{
    [Serializable()]
    class LayerObjects
    {
        public string Name { get; set; }
	    public string LayerPath { get; set; }
	
	    public LayerObjects(string N, string L) {
		    Name = N;
		    LayerPath = L;
	    }
    }
}
