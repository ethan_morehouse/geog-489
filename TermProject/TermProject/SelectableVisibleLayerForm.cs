﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.ArcMapUI;

namespace TermProject
{
    public partial class SelectableVisibleLayerForm : Form
    {
        public SelectableVisibleLayerForm()
        {
            InitializeComponent();
        }

        //close form
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //update layer attributes and close form
        private void btnOK_Click(object sender, EventArgs e)
        {
            IMxDocument pMxDoc = (IMxDocument)ArcMap.Application.Document;
            IEnumLayer pLayers = pMxDoc.FocusMap.get_Layers(); ;
            pLayers.Reset();
            ILayer pLayer = pLayers.Next();

            //using indexes instead of layer names allows for layers with the same name to be identified separately
            int selectIndex = 0;
            int visibleIndex = 0;

            while (pLayer != null)
            {
                
                if (this.cblVisible.CheckedIndices.Contains(visibleIndex))
                {
                    pLayer.Visible = true;
                }
                else
                {
                    pLayer.Visible = false;
                }
                visibleIndex++;

                if (pLayer is IFeatureLayer2)
                {
                    IFeatureLayer2 pFeatureLayer = (IFeatureLayer2)pLayer;
                    if (this.clbSelection.CheckedIndices.Contains(selectIndex))
                    {
                        pFeatureLayer.Selectable = true;
                    }
                    else
                    {
                        pFeatureLayer.Selectable = false;
                    }
                    selectIndex++;
                }
                pLayer = pLayers.Next();
            }
            pMxDoc.UpdateContents();
            pMxDoc.ActiveView.Refresh();
            this.Close();
        }

        private void btnVisibleAll_Click(object sender, EventArgs e)
        {
            AllOrNothing(2, true);
        }


        private void btnVisibleNone_Click(object sender, EventArgs e)
        {
            AllOrNothing(2, false);
        }

        private void btnSelectionAll_Click(object sender, EventArgs e)
        {
            AllOrNothing(1, true);
        }

        private void btnSelectionNone_Click(object sender, EventArgs e)
        {
            AllOrNothing(1, false);
        }
        
        private void btnSelectionInvert_Click(object sender, EventArgs e)
        {
            Invert(1);
        }
        
        private void btnVisibleInvert_Click(object sender, EventArgs e)
        {
            Invert(2);
        }

        private void AllOrNothing(int tab,bool status)
        {
            if (tab == 2)
            {
                for (int i = 0; i < this.cblVisible.Items.Count; i++)
                {
                    this.cblVisible.SetItemChecked(i, status);
                }
            }
            else if(tab == 1)
            {
                for (int i = 0; i < this.clbSelection.Items.Count; i++)
                {
                    this.clbSelection.SetItemChecked(i, status);
                }
            }
        }

        private void Invert(int tab)
        {
            if (tab == 2)
            {
                for (int i = 0; i < this.cblVisible.Items.Count; i++)
                {
                    this.cblVisible.SetItemChecked(i, !this.cblVisible.GetItemChecked(i));
                }
            }
            else if (tab == 1)
            {
                for (int i = 0; i < this.clbSelection.Items.Count; i++)
                {
                    this.clbSelection.SetItemChecked(i, !this.clbSelection.GetItemChecked(i));
                }
            }
        }
    }
}
