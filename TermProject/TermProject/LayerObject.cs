﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TermProject
{
    class LayerObject
    {
        public string name;
        public string path;

        public LayerObject(string name, string path)
        {
            this.name = name;
            this.path = path;
        }
    }
}
