﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TermProject
{
    public partial class LoadRastersFrom : Form
    {
        public LoadRastersFrom()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (clbCategories.CheckedItems.Count != 1 && string.IsNullOrEmpty(txtCategory.Text))
            {
                return;
            }
            
            string category = null;

            if (clbCategories.Enabled)
            {

                category = clbCategories.GetItemText(clbCategories.SelectedItem);
                RasterMethods.AddToExistingCategory(category);
            }
            else
            {
                category = txtCategory.Text;
                
                //string[] categories = clbCategories.Items.OfType<string>().ToArray();
                if (clbCategories.Items.OfType<string>().ToList<string>().Contains(category))
                {
                    RasterMethods.AddToExistingCategory(category);
                }

                RasterMethods.CreateCategory(category);
            }

            this.Close();

        }

        //disable checked list box if there is text in the text box
        private void txtCategory_TextChanged(object sender, EventArgs e)
        {
            if (this.txtCategory.Text.Length > 0)
            {
                this.clbCategories.Enabled = false;
            }
            else
            {
                this.clbCategories.Enabled = true;
            }
        }


        //only allow one item to be checked in checked list box
        private void clbCategories_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            for (int i = 0; i < clbCategories.Items.Count; i++)
            {
                if (i != e.Index)
                {
                    clbCategories.SetItemChecked(i, false);
                }
            }
        }

        private void btnErase_Click(object sender, EventArgs e)
        {
            if (!clbCategories.Enabled || clbCategories.CheckedItems.Count != 1)
            {
                return;
            }

            //delete all category entries in csv
            string category = clbCategories.GetItemText(clbCategories.SelectedItem);
            RasterMethods.DeleteCategory(category);

            //Clear items in Check list box:
            clbCategories.Items.Clear();

            //Reload if a cateogry stil exists
            if (File.Exists(RasterMethods.CsvSavePath))
            {
                List<string> reloadedCategories = RasterMethods.ReadCsv(0);
                foreach (string reloadCategory in reloadedCategories)
                {
                    clbCategories.Items.Add(reloadCategory);
                }
            }
        }
    }
}
