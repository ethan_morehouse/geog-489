﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Carto;

namespace TermProject
{
    public class RasterWizard : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        public RasterWizard()
        {
        }

        protected override void OnClick()
        {
            RasterMethods.pMxDoc = (IMxDocument)ArcMap.Application.Document;
            RasterMethods.pMap = RasterMethods.pMxDoc.FocusMap;
            RasterForm form = new RasterForm();
            List<string> categories = RasterMethods.ReadCsv(0);
            if (categories != null)
            {
                foreach (string category in categories)
                {
                    form.clbCategories.Items.Add(category);
                }
            }
            form.ShowDialog();

        }
        protected override void OnUpdate()
        {
            Enabled = ArcMap.Application != null;
        }
    }

}
