﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TermProject
{
    public partial class RasterForm : Form
    {
        public RasterForm()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            LoadRastersFrom loadform = new LoadRastersFrom();
            List<string> categories = RasterMethods.ReadCsv(0);
            if (categories != null)
            {
                foreach (string category in categories)
                {
                    loadform.clbCategories.Items.Add(category);
                }
            }

            loadform.ShowDialog();

            this.clbCategories.Items.Clear();

            List<string> reloadedCategories = RasterMethods.ReadCsv(0);
            if (reloadedCategories != null)
            {
                foreach (string category in reloadedCategories)
                {
                    this.clbCategories.Items.Add(category);
                }
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void clbCategories_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            for (int i = 0; i < clbCategories.Items.Count; i++)
            {
                if (i != e.Index)
                {
                    clbCategories.SetItemChecked(i, false);
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (clbCategories.CheckedItems.Count != 1)
            {
                return;
            }
            
            string category = clbCategories.GetItemText(clbCategories.SelectedItem);
            RasterMethods.AddRasterToMap(category);

            this.Close();
            
        }
    }
}
